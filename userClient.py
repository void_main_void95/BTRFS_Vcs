import json
import os
import datetime
import socket as sck
import threading
import sys
import lib.DbInterface as libDatabase

class Client():
    def __init__(self):
        self.host = "192.168.122.147"
        self.port = 6660
        self.username = "pippo"
        self.password = "localhost"


    def get_log(self, log_name):
        message = dict()
        message["command"] = "getLog"
        message["token"] = self.current_token
        message["payload"] = dict()
        message["payload"]["name"] = log_name

        self.create_socket()
        fd_socket = self.socket.makefile("rw")
        fd_socket.write(json.dumps(message)+"\n")
        fd_socket.flush()
        response = fd_socket.readline()
        fd_socket.flush()
        fd_socket.close()
        response.replace('\n','')
        response = json.loads(response)
        if response["command"] == "getSuccess":
            print(response["payload"])
            self.socket.close()
        elif response["command"] == "tokenInvalid":
            print("InvaliToken")
            self.authenticate(self.username, self.password)
            self.socket.close()
            self.get_snapshot(owner)
        else:
            print("Failed")
            self.socket.close()

    def get_snapshot(self, owner):
        message = dict()
        message["command"] = "getSnapshot"
        message["token"] = self.current_token
        message["payload"] = dict()
        message["payload"]["owner"] = owner

        self.create_socket()
        fd_socket = self.socket.makefile("rw")
        fd_socket.write(json.dumps(message)+ "\n")
        fd_socket.flush()
        response = fd_socket.readline()
        fd_socket.flush()
        fd_socket.close()
        response.replace('\n', '')
        response = json.loads(response)
        if response["command"] == "getSuccess":
            print(response["payload"])
            self.socket.close()
        elif response["command"] == "tokenInvalid":
            self.authenticate(self.username, self.password)
            self.socket.close()
            print("InvaliToken")
            self.get_snapshot(owner)
        else:
            print("Failed")
            self.socket.close()

    def plan_restore(self, fs_id):
        message = dict()
        message["command"] = "planRestore"
        message["token"] = self.current_token
        message["payload"] = dict()
        message["payload"]["fs_id"] = fs_id

        self.create_socket()
        fd_socket = self.socket.makefile("rw")
        fd_socket.write(json.dumps(message) + "\n")
        fd_socket.flush()
        response = fd_socket.readline()
        fd_socket.flush()
        fd_socket.close()
        response.replace('\n', '')
        response = json.loads(response)
        if response["command"] == "planSuccess":
            print(response["payload"])
            self.socket.close()
        elif response["command"] == "tokenInvalid":
            self.authenticate(self.username, self.password)
            self.socket.close()
            print("InvaliToken")
            self.get_snapshot(owner)
        else:
            print("Failed")
            self.socket.close()

    def delete_snapshot(self, fs_id):
        message = dict()
        message["command"] = "deleteSnapshot"
        message["token"] = self.current_token
        message["payload"] = dict()
        message["payload"]["fs_id"] = fs_id

        self.create_socket()
        fd_socket = self.socket.makefile("rw")
        fd_socket.write(json.dumps(message) + "\n")
        fd_socket.flush()
        response = fd_socket.readline()
        fd_socket.flush()
        fd_socket.close()
        response.replace('\n', '')
        response = json.loads(response)
        if response["command"] == "deleteSuccess":
            print(response["payload"])
            self.socket.close()
        elif response["command"] == "tokenInvalid":
            self.authenticate(self.username, self.password)
            self.socket.close()
            print("InvaliToken")
            self.get_snapshot(owner)
        else:
            print("Failed")
            self.socket.close()

    def make_snapshot(self, name):
        message = dict()
        message["command"] = "makeSnapshot"
        message["token"] = self.current_token
        message["payload"] = dict()
        message["payload"]["name"] = name

        self.create_socket()
        fd_socket = self.socket.makefile("rw")
        fd_socket.write(json.dumps(message)+ "\n")
        response = fd_socket.readline()
        fd_socket.flush()
        fd_socket.close()
        response.replace('\n','')
        response = json.loads(response)
        if response["command"] == "makeSuccess":
            print(response["payload"])
            self.socket.close()
        elif response["command"] == "tokenInvalid":
            print("InvaliToken")
            self.authenticate(self.username, self.password)
            self.socket.close()
            self.get_snapshot(owner)
        else:
            print("Failed")
            self.socket.close()

    def authenticate(self, username, password):
        message = dict()
        message["command"] = "auth"
        message["payload"] = dict()
        message["payload"]["username"] = username
        message["payload"]["password"] = password
        self.create_socket()
        fd_socket = self.socket.makefile("rw")
        fd_socket.write(json.dumps(message) + "\n")
        fd_socket.flush()
        response = fd_socket.readline()
        response.replace('\n', '')
        response = json.loads(response)
        if response["command"] == "setToken":
            self.current_token = response["payload"]["token"]
        else:
            print("Unauthorized")

    def create_socket(self):
        self.socket = sck.socket(sck.AF_INET, sck.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def close(self):
        self.socket.shutdown(sck.SHUT_RDWR)