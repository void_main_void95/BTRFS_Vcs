import subprocess

def execute_bash_command(command):
    child_process = subprocess.Popen(command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = child_process.communicate()
    return child_process.returncode, output


