import urwid
import lib.BtrfsRecovery as libRecovery
import lib.DbInterface as libDatabase
import lib.Logger as libLogger
from lib.exception import *
import os

class Login_menu():
    def __init__(self):
        self.menu_top = self.login_menu("Login")

    def login_menu(self, title, first_time= True):

        unix_name = urwid.Edit("Unix_name\n\n")
        password = urwid.Edit(caption="Password:\n\n", mask="")
        login = self.menu_button(u'Login', self.login)
        urwid.connect_signal(unix_name, 'change', self.get_user)
        urwid.connect_signal(password, 'change', self.get_password)
        exit = self.menu_button("Exit", self.exit_program)
        if first_time:
            body = [urwid.Text(title), urwid.Divider(), unix_name, urwid.Divider(), password, urwid.Divider(), login, urwid.Divider(), exit]
        else:
            error = urwid.Text("Wrong unix_name or password! Please retry")
            body = [urwid.Text(title), urwid.Divider(), unix_name, urwid.Divider(), password, urwid.Divider(), error, urwid.Divider(), login,
                    urwid.Divider(), exit]

        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')
        return listBox

    def login(self, button):
        logger.write_recovery_menu_info("Login required from: " + self.current_user)
        if db.authenticate(self.current_user, self.current_password) == True:
            logger.write_recovery_menu_info("Login success")
            self.current_password = ""
            render_window(Recovery_menu(self.current_user).menu_top)
        else:
            logger.write_recovery_menu_error("Authernticating user", "Unauthorized")
            self.current_user = ""
            self.current_password = ""
            self.menu_top = self.login_menu("Login", first_time=False)
            render_window(self.menu_top)

    def menu_button(self, caption, callback):
        button = urwid.Button(caption)
        urwid.connect_signal(button, 'click', callback)
        return urwid.AttrMap(button, 'banner', focus_map='reversed')

    def get_user(self, edit, new_edit_text):
        self.current_user = new_edit_text

    def get_password(self, edit, new_edit_text):
        self.current_password = new_edit_text

    def exit_program(self, button):
        exit()

class Recovery_menu():
    def __init__(self, logged_user):
        self.manual_restore_snap_id = ""
        self.logged_user = logged_user
        if logged_user == "recovery":
            title = "Restore root environment"
            envs = db.get_env_ids()
            choices = list()
            env_detail = list()
            found = False
            if envs != "NoValue":
                for env in envs:
                    details = db.environment_detail(env)
                    restore_plan = db.get_recovery_plan(self.logged_user)
                    if details["owner"] == "recovery":
                        env_id = urwid.Text("ID: " + details["env_id"])
                        name = urwid.Text("Name: " + details["name"])
                        creation_date = urwid.Text("Creation date: " +details["creation_date"])
                        owner = urwid.Text("Owner : " +details["owner"])
                        status = urwid.Text("Status: "+ details["status"])
                        rollback_target = urwid.Text("Rollback target: " + restore_plan["snap_id"])
                        env_detail = [env_id, name, creation_date, owner, status, rollback_target]
                        for snapshot in details["snapshots"]:
                            choices.append(self.menu_button(snapshot["fs_id"], self.snap_chosen))
                            found = True
                            break

            if found == True:
                menu = [
                        self.menu_button("Automatic rollback", self.item_chosen),
                        self.root_restore(choices=choices, found=found),
                        urwid.Divider(),
                        self.menu_button("Back", self.back)
                    ]

            else :
                logger.write_recovery_menu_info("No env found")
                menu = []

        else:
            title = "Restore home environment"
            choices = list()
            env_detail = list()
            envs = db.get_env_ids()
            found = False
            if envs != "NoValue":
                for env in envs:
                    details = db.environment_detail(env)
                    restore_plan = db.get_recovery_plan(self.logged_user)
                    if details["owner"] == self.logged_user and details["owner"] != "recovery":
                        env_id = urwid.Text("ID: " + details["env_id"])
                        name = urwid.Text("Name: " + details["name"])
                        creation_date = urwid.Text("Creation date: " + details["creation_date"])
                        owner = urwid.Text("Owner : " + details["owner"])
                        status = urwid.Text("Status: " + details["status"])
                        rollback_target = urwid.Text("Rollback target: "+ restore_plan["snap_id"])
                        env_detail = [env_id, name, creation_date, owner, status, rollback_target]
                        for snapshot in details["snapshots"]:
                            choices.append(self.menu_button(snapshot["fs_id"], self.snap_chosen))
                            found = True
                            break
            if found == True:
                menu = [
                        self.menu_button("Automatic rollback", self.item_chosen),
                        self.home_restore(choices=choices, found=found),
                        urwid.Divider(),
                        self.menu_button("Back", self.back)
                    ]
            else :
                logger.write_recovery_menu_info("No env found")
                menu = []


        self.menu_top = self.menu(u'BTRFS_vcs', [
                self.env_menu(title, menu, env_detail),
                # self.menu_button('Create new user snapshot', self.new_user_snapshot),
                self.log_menu("Log"),
                self.menu_button('Exit', self.exit_program)])

    def menu_button(self, caption, callback):
        button = urwid.Button(caption)
        urwid.connect_signal(button, 'click', callback)
        return urwid.AttrMap(button, 'banner', focus_map='reversed')

    def env_menu(self, caption, choices, env_details):
        body = [urwid.Text(caption), urwid.Divider()]
        if len(env_details) != 0:
            body.extend(env_details)
            body.append(urwid.Divider())
        if len(choices) != 0:
            body.extend(choices)
        else:
            back = self.menu_button("Back", self.back)
            body2 = [urwid.Text("No environment for this user"), urwid.Divider(), back,
                    urwid.Divider()]
            body.extend(body2)
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')

        def open_menu(button):
            return top.open_box(listBox)
        return self.menu_button(caption, open_menu)

    def log_menu(self, caption):
        back = self.menu_button("Back", self.back)
        body = [urwid.Text(caption), urwid.Divider(), back, urwid.Divider()]
        log = open("./recovery_menu.log", "r")
        log_file = log.read()
        log.close()
        body.append(urwid.Text(log_file))
        body.append(back)
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')

        def open_menu(button):
            return top.open_box(listBox)

        return self.menu_button(caption, open_menu)


    def menu(self, title, choices):
        body = [urwid.Text(title), urwid.Divider()]
        body.extend(choices)
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')
        return listBox

    def root_restore(self, choices, found):
        captions = "Manual rollback"

        def open_menu(button):
            if found != True:
                return top.open_box(self.no_snapshot_menu(captions, choices))
            else:
                return top.open_box(self.menu_snapshot(captions, choices))

        return self.menu_button([captions, u''], open_menu)

    def home_restore(self, choices, found):
        captions = "Manual rollback"

        def open_menu(button):
            return top.open_box(self.menu_snapshot(captions, choices))

        return self.menu_button([captions, u''], open_menu)

    def menu_snapshot(self, title, choices):
        back = self.menu_button("Back", self.back)
        body = [urwid.Text(title), urwid.Divider(), urwid.Text("Select a Snapshot to restore"), urwid.Divider(), back, urwid.Divider()]
        body.extend(choices)
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')
        return listBox

    def diag_no_env(self):
        back = self.menu_button("Back", self.back)
        body = [urwid.Text("No environment for this user"), urwid.Divider(), back,
                urwid.Divider()]
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')
        return urwid.BoxAdapter(listBox, 10)

    def automatic_restore(self, button):
        recovery_plan = db.get_recovery_plan(self.logged_user)
        try:
            if recovery_plan != "NoValue":
                osVolume = db.get_config("OsVolume")
                env_id = db.get_env_from_snap_id(recovery_plan["snap_id"])
                #recovery.restore_snapshot(id=recovery_plan["snap_id"], osVolume=osVolume, user=self.logged_user)
                #db.delete_env_snapshot(env_id=env_id, snap_id=restore_plan["snap_id"])
                #db.delete_snapshot(restore_plan["snap_id"])
                logger.write_recovery_menu_info("Env: " + env_id + " -> Rolling back success. " + recovery_plan["snap_id"] + " restored" )
                self.restore_complete(button)

        except DeleteException as e:
            logger.write_recovery_menu_error("Rolling back", e.get_error())
            try:
                recovery_volume = db.get_config("RecoveryVolume")
                recovery.emergency_umount(recovery_volume)

            except UmountErrorException as e:
                logger.write_recovery_menu_error("Emergency umount", e.get_error())
            finally:
                self.restore_failed(button)

        except MountErrorException as e:
            logger.write_recovery_menu_error("Rolling back", e.get_error())
            try:
                recovery_volume = db.get_config("RecoveryVolume")
                recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                logger.write_recovery_menu_error("Emergency umount", e.get_error())
            finally:
                self.restore_failed(button)

        except UmountErrorException as e:
            logger.write_recovery_menu_error("Rolling back", e.get_error())
            try:
                recovery_volume = db.get_config("RecoveryVolume")
                recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                logger.write_recovery_menu_error("Emergency umount", e.get_error())
            finally:
                self.restore_failed(button)

        except MoveException as e:
            logger.write_recovery_menu_error("Rolling back", e.get_error())
            try:
                recovery_volume = db.get_config("RecoveryVolume")
                recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                logger.write_recovery_menu_error("Emergency umount", e.get_error())
            finally:
                self.restore_failed(button)

    def snap_chosen(self, button):
        details = db.get_snapshots_detail(button.label)
        name = urwid.Text([u'Name: ', details["name"]])
        creation_date = urwid.Text([u'Creation date: ', details["creation_date"]])
        volid = urwid.Text([u'Volid: ', details["volid"]])
        type = urwid.Text([u'Type: ', details["type"]])
        owner = urwid.Text([u'Owner: ', details["owner"]])
        response = urwid.Text([u'You chose: ', button.label])
        self.manual_restore_snap_id = button.label
        done = self.menu_button(u'Confirm', self.manual_restore)
        back = self.menu_button(u'Back', self.back)
        body = [response, urwid.Divider(), name, volid,
                type, creation_date, owner, urwid.Divider(),
                done, urwid.Divider(), back]
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')
        return top.open_box(listBox)

    def manual_restore(self, button):
        db.update_recovery_plan(user=self.logged_user, snap_id=self.manual_restore_snap_id)
        self.automatic_restore(button)

    def item_chosen(self, button):
        response = urwid.Text([u'You chose ', button.label, u'\n'])
        done = self.menu_button(u'Confirm', self.automatic_restore)
        back = self.menu_button(u'Back', self.back)
        body = [response, urwid.Divider(), done, urwid.Divider(), back]
        listBox = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        urwid.AttrMap(listBox, 'bg')
        return top.open_box(listBox)

    def restore_complete(self, button):
        self.back(button)
        response = urwid.Text(["Rollback success. Please reboot"])
        back = self.menu_button(u'Back', self.main_back)
        top.open_box(urwid.Filler(urwid.Pile([response, urwid.Divider(), back])), "small")

    def restore_failed(self, button):
        self.back(button)
        response = urwid.Text(["Failed to rollback environments\n See log for details"])
        back = self.menu_button(u'Back', self.main_back)
        top.open_box(urwid.Filler(urwid.Pile([response, urwid.Divider(), back])), "small")

    def new_user_snapshot(self, button):
        '''
        name = urwid.Edit("Insert snapshot name:\n\n")
        done = self.menu_button(u'Create', self.show_name)
        urwid.connect_signal(name, 'change', self.on_ask_change)
        back = self.menu_button("Back", self.back)
        top.open_box(urwid.Filler(urwid.Pile([name, urwid.Divider(), done, urwid.Divider(), back])), "normal")
        '''

    def back(self, button):
        if top.box_level != 1:
            top.original_widget = top.original_widget[0]
            top.box_level -= 1

    def main_back(self, button):
        render_window(Recovery_menu(self.logged_user).menu_top)

    def exit_program(self, button):
        exit()

    def automatic_snapshot_restore(self):
        pippo = "2"


class CascadingBoxes(urwid.WidgetPlaceholder):
    max_box_levels = 10

    def __init__(self, box):
        super(CascadingBoxes, self).__init__(urwid.SolidFill())
        self.box_level = 0
        self.open_box(box)

    def open_box(self, box, dim="big"):
        if dim == "big":
            width = 80
            height = 80
        elif dim == "normal":
            width = 60
            height = 60
        elif dim == "small":
            width = 40
            height = 40
        self.original_widget = urwid.Overlay(urwid.LineBox(box),
            self.original_widget,
            align='center', width=('relative', width),
            valign='middle', height=('relative', height),
            min_width=24, min_height=8,
            left=self.box_level * 3,
            right=(self.max_box_levels - self.box_level - 1) * 3,
            top=self.box_level * 2,
            bottom=(self.max_box_levels - self.box_level - 1) * 2)
        self.box_level += 1

def render_window(window):
    global top, loop
    top = CascadingBoxes(window)
    loop.widget = urwid.AttrMap(top, 'bg')
    loop.palette = [('banner', 'black', 'light gray'),
                             ('streak', 'black', 'dark red'),
                             ('bg', 'white', 'light blue'),
                             ('invisible', "light blue", 'light blue')]
    loop.draw_screen()

def exit():
    db.close()
    logger.close()
    raise urwid.ExitMainLoop()

local_path = os.path.dirname(os.path.abspath(__file__))
db = libDatabase.DbInteface("recovery.db")
logger = libLogger.RecoveryMenuLogger("./")
recovery = libRecovery.BtrfsRecovery("recovery_menu")
menu =Login_menu()
top = CascadingBoxes(menu.menu_top)
loop = urwid.MainLoop(urwid.AttrMap(top, 'bg'), palette=[('banner', 'black', 'light gray'),
                             ('streak', 'black', 'dark red'),
                             ('bg', 'white', 'light blue'),
                             ('invisible', "light blue", 'light blue')])
loop.run()