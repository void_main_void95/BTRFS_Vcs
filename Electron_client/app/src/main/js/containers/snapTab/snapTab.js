import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia, Paper} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import  { actions as auth }  from '../../actions/auth'
import classnames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import SwipeableViews from 'react-swipeable-views';
import {env_details} from '../../RecoveryFunction'
import SnapshotsExpand from '../snapshotsExpand/snapshotsExpand'
import WatcherExpand from '../watcherExpand/watcherExpand'
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import AddIcon from '@material-ui/icons/Add';
import grey from '@material-ui/core/colors/grey'


const styles ={
  details_card: {
    maxHeight: "100%",
    maxWidth: "100%",
    marginLeft: 40,
    marginRight: 40,
    marginTop: 20,
    background: grey[600],
  },
  snapshots_card: {
    maxHeight: "100%",
    maxWidth: "100%",
    marginLeft: 40,
    marginRight: 40,
    marginTop: 30,
    background: grey[600],
  },
  paper_grid:{
    flexGrow: 1,
    justify: "center",
    marginTop: 60
  },
  string: {
    marginLeft: 10,
    marginTop: 5,
    marginBottom: 5
  },
  add_watcher_button: {
    marginTop: 15,
  }
}


class SnapTab extends React.Component{

  constructor(props){
    super(props)
  }

  add_watcher_press(){
    console.log("ADD")
  }
  render(){
    if(this.props.env_catched){
  return (
            <Grid container style={styles.paper_grid}>
              <Grid item xs={6} sm={6} style={{minHeight: "20vh"}}>
                  <Card style={styles.details_card} elevation={12}>
                  <CardContent>
                      <Grid container style={{textAlign: "left"}}>
                        <Grid item xs={12}>
                          <Typography style={styles.string} variant="title"> ID: {this.props.env.env_id}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                          <Typography style={styles.string} variant="title"> Name: {this.props.env.name}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                          <Typography style={styles.string} variant="title"> Creation date: {this.props.env.creation_date}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                          <Typography style={styles.string} variant="title"> Owner: {this.props.env.owner}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                          <Typography style={styles.string} variant="title"> Status: {this.props.env.status}</Typography>
                        </Grid>
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={6} sm={6}>
                    <Card style={styles.details_card} elevation={12}>
                    </Card>
                  </Grid>
                <Grid item xs={6} sm={6} style={{minHeight: "60vh"}}>
                  <Card style={styles.snapshots_card} elevation={12}>
                    <CardContent>
                  <SnapshotsExpand snapshots={this.props.env.snapshots} />
                    </CardContent>
                  </Card>
                </Grid>
                  <Grid item xs={6} sm={6} style={{minHeight: "60vh"}}>
                    <Card style={styles.snapshots_card} elevation={12}>
                      <CardContent>
                      <WatcherExpand watchers={this.props.env.watchers} />
                          <Button variant="fab" color="primary" aria-label="add" style={styles.add_watcher_button} onClick={this.add_watcher_press}>
                              <AddIcon />
                          </Button>
                      </CardContent>
                    </Card>
                 </Grid>
             </Grid>
  )
}
else{
  return(
      <Paper style={styles.root} elevation={14}>
      </Paper>
)
}
}

}

function mapStateToProps(state){
  return {
    user: state.LoginReducer.user,
  }
}

  function mapDispatchToProps(dispatch) {
      return({
          get_env_details: () => {dispatch(envDetails.get_env_details())},
          details_consumed: () => {dispatch(envDetails.details_consumed())}
      })
  }


export default connect(mapStateToProps, mapDispatchToProps)(SnapTab);
