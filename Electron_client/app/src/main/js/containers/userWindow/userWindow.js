import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import  { actions as auth }  from '../../actions/auth'
import  { actions as envAction }  from '../../actions/environmentAction'
import {login} from '../../RecoveryFunction'
import classnames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import SwipeableViews from 'react-swipeable-views';
import SnapTab from '../snapTab/snapTab'
import {get_environments} from '../../RecoveryFunction'
import deepPurple from '@material-ui/core/colors/deepPurple'
import blue from '@material-ui/core/colors/blue'
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import HomeIcon from '@material-ui/icons/Home';
import AddToQueueIcon from '@material-ui/icons/AddToQueue'
import AddEnvironment from '../addEnvironment/AddEnviroment'

const theme = createMuiTheme({
  palette: {
    type: 'light', // Switching the dark mode on is a single property value change.
    primary: deepPurple,
    secondary: blue,
  },
  typography:{
    body2: {
      color: "white",
    },
  },
});


class UserWindow extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.get_envs()
        get_environments(this.props.user.token)
    }

    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({value});
    };

    handleChangeIndex = index => {
        this.setState({value: index});
    };


    render() {



        if (!this.props.logged) {
            return (<Redirect to="/"/>)
        }
        return (
            <MuiThemeProvider theme={theme}>
                <div>
                    <AppBar>
                        <Tabs
                            value={this.state.value}
                            onChange={this.handleChange}
                            fullWidth
                        >
                            {this.props.requireSuccess ? (this.props.envs.map(n => {
                                let check_home
                                if (n.owner === "recovery"){
                                    check_home = false
                                }
                                else{
                                    check_home = true
                                }

                                return (

                                    <Tab icon={check_home ? (<HomeIcon />): (<FolderSpecialIcon/>)}
                                         label={check_home ? ("HOME ".concat(n.owner)) : ("ROOT")}
                                    />

                                )
                            })) : (<Tab label={"NoEnv"}/>)}

                            {this.props.only_recovery && !this.props.user.isRoot ? (<Tab icon={<AddToQueueIcon/>} label={"NEW"}/>) : (<div />)}
                        </Tabs>
                    </AppBar>

                    <SwipeableViews
                        index={this.state.value}
                        onChangeIndex={this.handleChangeIndex}>

                        {this.props.requireSuccess ? (this.props.envs.map(n => {
                            return (
                                <SnapTab env={n} env_catched={true}/>
                            );
                        })) : (<div />)}
                        {this.props.only_recovery && !this.props.user.isRoot ? (
                            <AddEnvironment/>

                        ) : (<div />)}

                    </SwipeableViews>
                </div>
            </MuiThemeProvider>
        )

    }
}



  function mapStateToProps(state){
  	return {
      envs: state.EnvironmentReducer.envs,
      user: state.LoginReducer.user,
      logged: state.LoginReducer.logged,
      requireSuccess: state.EnvironmentReducer.requireSuccess,
      only_recovery: state.EnvironmentReducer.only_recovery,
  	}
  }

    function mapDispatchToProps(dispatch) {
        return({
            get_envs: () => {dispatch(envAction.get_envs())},
        })
    }


export default connect(mapStateToProps, mapDispatchToProps)(UserWindow);
