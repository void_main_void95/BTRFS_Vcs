import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia, Paper} from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import grey from '@material-ui/core/colors/grey'

class SnapshotExpandItem extends React.Component{

  render(){
    return(
      <ExpansionPanel style={{background: grey[500]}}>
       <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
         <Typography>{this.props.snapshot.name}</Typography>
       </ExpansionPanelSummary>
       <ExpansionPanelDetails>
         <Grid container style={{flexGrow: 1, alignContent: "center"}}>
          <Grid item sm={6} style={{marginTop:5}}>
            <Typography>{("ID: ").concat(this.props.snapshot.fs_id)}</Typography>
          </Grid>
          <Grid item sm={6} style={{marginTop:5}}>
            <Typography>{("Volid: ").concat(this.props.snapshot.volid)}</Typography>
          </Grid>
          <Grid item sm={6} style={{marginTop:5}}>
            <Typography>{("Type: ").concat(this.props.snapshot.type)}</Typography>
          </Grid>
          <Grid item sm={6} style={{marginTop:5}}>
            <Typography>{("Creation date: ").concat(this.props.snapshot.creation_date)}</Typography>
          </Grid>
          <Grid item sm={6} style={{marginTop:5}}>
            <Typography>{("Owner: ").concat(this.props.snapshot.owner)}</Typography>
          </Grid>
        </Grid>
       </ExpansionPanelDetails>
     </ExpansionPanel>
    )
  }
}

export default SnapshotExpandItem
