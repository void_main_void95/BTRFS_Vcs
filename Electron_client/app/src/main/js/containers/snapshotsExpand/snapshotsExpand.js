import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia, Paper} from '@material-ui/core';
import SnapshotExpandItem from './snapshotExpandItem';

class SnapshotsExpand extends React.Component{

  render() {
    return(
    <div>
      <Typography style={{marginBottom: 10}}>Snapshots</Typography>
      <div width="100%">
      {this.props.snapshots.map(n => {
      return (
          <SnapshotExpandItem snapshot={n} />
      );
    })}
    </div>
    </div>
  );
  }


}

export default SnapshotsExpand;
