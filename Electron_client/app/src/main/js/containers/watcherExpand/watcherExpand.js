import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia, Paper} from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import ListSubheader from '@material-ui/core/ListSubheader';
import WatcherExpandItem from './watcherExpandItem'
import AddIcon from '@material-ui/icons/Add';



function watcherAdd(){
  console.log("Add")
}

class WatcherExpand extends React.Component{
  render(){
    return(
      <div>
          <Typography style={{marginBottom: 10}}>Watchers</Typography>
          <div width="100%">
           {this.props.watchers.map(n => {
             return(
             <WatcherExpandItem watcher={n} />
              )
           })}
           </div>
           </div>
    )
  }
}

export default WatcherExpand
