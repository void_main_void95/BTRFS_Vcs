import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia, Paper} from '@material-ui/core'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import grey from '@material-ui/core/colors/grey'


class WatcherExpandItem extends React.Component{

  render(){
      return (
          <ExpansionPanel style={{background: grey[500]}}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography>{this.props.watcher.file}</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                  <Grid container style={{flexGrow: 1, alignContent: "center"}}>
                      <Grid item sm={6} style={{marginTop:5}}>
                          <Typography>{("Key: ").concat(this.props.watcher.key)}</Typography>
                      </Grid>
                      <Grid item sm={6} style={{marginTop:5}}>
                          <Typography>{("Permission Filter: ").concat(this.props.watcher.permission_filter)}</Typography>
                      </Grid>
                      <Grid item sm={6} style={{marginTop:5}}>
                          <Typography>{("Owner: ").concat(this.props.watcher.owner)}</Typography>
                      </Grid>
                  </Grid>
              </ExpansionPanelDetails>
          </ExpansionPanel>
      );
  }
}

export default WatcherExpandItem
