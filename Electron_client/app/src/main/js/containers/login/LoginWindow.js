import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import  { actions as auth }  from '../../actions/auth'
import {login} from '../../RecoveryFunction'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'

const style = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '90vh',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0
  },
  card: {
    maxWidth: 700,
    background: grey[600]
  },
  avatar: {
    margin: '1em',
    textAlign: 'center '
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '5vh',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0,
  },
  input: {
    padding: 1
  },
  logo: {
    maxWidth: '50%',
    maxHeight: '50%'
  },
  button: {
    marginTop: 50,
    background: "#47738c"
  },

}

const theme = createMuiTheme({
  palette: {
    type: 'light', // Switching the dark mode on is a single property value change.
  },
});

class LoginComponent extends React.Component {
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  componentWillMount(){
    console.log(this.props)
  }
  handleSubmit(){
    this.props.loginRequest()
    login({
      unix_name: this.state.unix_name,
      password: this.state.password,
      token: ''
    })
  }

  handleChange = field => evt => {
    let state = {}
    state[field] = evt.target.value
    this.setState(state)
  }

  render() {
    let logged= this.props.logged
    let user = this.props.user

    if (logged){
      console.log(this.props.user.token)
      return(<Redirect to= "/main2"/>)
    }


    return (
      <MuiThemeProvider theme={theme}>
      <Grid container style={style.container}>
        <Grid item xs={12} sm={12} style={{minWidth: 200}}>
          <Card style={style.card}>
            <img src="./res/images/linux.svg" style={style.logo}/>
            <CardContent>

              <form noValidate autoComplete="off" onSubmit={this.handleSubmit} style={style.form}>
                <TextField
                  id="unix_name"
                  label="Unix name"
                  value={this.props.unix_name}
                  margin="normal"
                  onChange={this.handleChange('unix_name')}
                  style={style.input}
                />
                <TextField
                  id="password"
                  label="Password"
                  value={this.props.password}
                  margin="normal"
                  type={'password'}
                  style={style.input}
                  onChange={this.handleChange('password')}
                />
                <Button variant="raised" color="primary" onClick={this.handleSubmit}>
                  {this.props.loading ? <CircularProgress size={18} /> : 'Login'}
                </Button>
              </form>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state){
  console.log(state)
	return {
		user: state.LoginReducer.user,
    loading: state.LoginReducer.loading,
    fetching: state.LoginReducer.fetching,
    logged: state.LoginReducer.logged
	}
}

  function mapDispatchToProps(dispatch) {
      return({
          loginSuccess: (user) => {dispatch(auth.loginSuccess(user))},
          loginFailure: (error) => {dispatch(auth.loginFailure(error))},
          loginRequest: () => {dispatch(auth.loginRequest())}
      })
  }


export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);
