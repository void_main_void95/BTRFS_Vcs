import { Component } from 'react';
import React from 'react';
import { arrayOf, shape, string, func, oneOfType, object, bool} from 'prop-types';
import { memoizeWith } from 'ramda';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import InsertDriveFile from '@material-ui/icons/InsertDriveFile';
import Folder from '@material-ui/icons/Folder'
import Checkbox from '@material-ui/core/Checkbox';
import {connect} from "react-redux";
import {actions as watcherAction} from "../../actions/watchersAction";
import {WatchersReducer} from "../../reducers/watchersReducer";

/** Prop-type for a recursive data structure */
const tree = {
    value: string.isRequired,
    label: string.isRequired,
    checked: bool.isRequired,
};

Object.assign(tree, {
    nodes: arrayOf(oneOfType([shape(tree), string])),
});

const styles = theme => ({
    panel: {
        width: '100%',
        paddingRight: 0,
        paddingLeft: 0,
    },
    panelSummary: {
        padding: 0,
        marginLeft: theme.spacing.unit,
    },
    panelDetails: {
        padding: 0,
        display: 'block',
    },
    panelExpanded: {
        margin: 0,
        '&:before': {
            opacity: 0,
        },
    },
    childPanel: {
        '&:before': {
            opacity: 0,
        },
    },
    text: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'noWrap',
        maxWidth: '75vw',
        verticalAlign: "middle",

    },
    icon: {
        verticalAlign: "middle",
        marginRight: 5,
    },
    expandIcon: {},
});

/**
 * Render a tree view.
 */
class MuiTreeView extends Component {
    constructor(props){
        super(props)

        this.state ={
            tree: props.tree
        }
        this.handleChange = this.handleChange.bind(this)

    }

    static propTypes = {
        /** The data to render as a tree view */
        tree: arrayOf(shape(tree)).isRequired,
        /** Callback function fired when a tree leaf is clicked. */
        onLeafClick: func,
        /** A search term to refine the tree */
        searchTerm: string,
        /** Properties applied to the ExpansionPanelSummary element. */
        expansionPanelSummaryProps: object,
        /** Properties applied to the ExpansionPanelDetails element. */
        expansionPanelDetailsProps: object,
        /** Properties applied to the ListItem element. */
        listItemProps: object,

        state: object,
    };

    static defaultProps = {
        searchTerm: null,
        onLeafClick: null,
        expansionPanelSummaryProps: null,
        expansionPanelDetailsProps: null,
        listItemProps: null,

    };

    createFilteredTree = memoizeWith(
        (tree, searchTerm) => `${tree.toString()}-${searchTerm}`,
        (tree, searchTerm) => (searchTerm ? this.filter(tree) : tree)
    );

    handleLeafClick = (value, parent) => {
        if (this.props.onLeafClick) {
            this.props.onLeafClick(value, parent);
        }
    };



    handleChange = node=> event => {
      this.updateTree(node, event.target.checked)
    };

    updateTree = (node, isChecked) => {
        let new_state = Object.assign({}, this.state)
        let tree = new_state.tree
        tree.forEach(n => {
            this.updateNode(n, node, isChecked)
        } )
        this.setState({tree: tree})
    }

    updateNode = (node, new_node, isChecked) => {
        if (node.label === new_node.label) {
            node.checked = isChecked
            if (!this.isLeaf(node)) {
                node.nodes.forEach(n => {
                    this.updateNode(n, n, isChecked)
                })
            }
        }
        else{
            if (!this.isLeaf(node)) {
                let checked = 0
                let unchecked = 0
                node.nodes.forEach(n => {
                        this.updateNode(n, new_node, isChecked)

                })
                node.nodes.forEach(n => {
                    if (n.checked || n.indeterminate){
                        checked +=1
                    }
                    else{
                        unchecked +=1
                    }

                })
                if (checked === node.nodes.length && unchecked === 0){
                    node.indeterminate = false
                    node.checked = true
                }
                else if (unchecked !== 0 && checked !== 0){
                    node.indeterminate = true
                }
                else{
                    node.indeterminate = false
                }
            }
        }

    }

    renderNode = (node, parent, depth = 0, selected=true) => {
        const {
            theme: {
                spacing: { unit },
            },
            classes,
            searchTerm,
            onLeafClick: _,
            expansionPanelSummaryProps,
            expansionPanelDetailsProps,
            listItemProps,
            ...props
        } = this.props;
        const value = this.getNodeValue(node);
        const isLeaf = this.isLeaf(node);
        const textIndent = isLeaf
            ? 2*depth * unit + unit + (parent ? unit : 0)
            : unit * depth + unit + (parent ? unit : 0);

        if (isLeaf && searchTerm && !value.includes(searchTerm)) {
            return null;
        }

        if (isLeaf) {
            return (
                <ListItem
                    disableGutters
                    style={{ textIndent }}
                    key={value}
                    id={value}
                    value={value}
                    onClick={() => this.handleLeafClick(value, parent)}
                    className={classes.itemListActive}
                    {...listItemProps}>

                    <div className={classes.text}>
                        <Checkbox value={node.value} onChange={this.handleChange(node)} checked={node.checked}/>
                        <InsertDriveFile className={classes.icon}/>

                        {node.label}

                        </div>

                </ListItem>

            );
        }


        const expansionPanelClasses = {
            expanded: classes.panelExpanded,
            ...(parent ? { root: classes.childPanel } : null),
        };


        this.props.getChecked(this.state)
        return (

            <ExpansionPanel
                className={classes.panel}
                classes={expansionPanelClasses}
                style={{ textIndent }}
                key={node.value}
                elevation={0}
                {...props}>
                <ExpansionPanelSummary
                    className={classes.panelSummary}
                    classes={{ expandIcon: classes.expandIcon }}
                    expandIcon={<KeyboardArrowDown />}
                    {...expansionPanelSummaryProps}>

                    <div className={classes.text} style={{paddingLeft: depth * 16}}>
                        <Checkbox value={node.value} onChange={this.handleChange(node)} checked={node.checked} indeterminate={node.indeterminate}/>
                        <Folder className={classes.icon} />

                        {node.label}
                        </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails
                    className={classes.panelDetails}
                    {...expansionPanelDetailsProps}>
                    {node.nodes.map(l => this.renderNode(l, node, depth + 1, selected))}
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    };

    isLeaf(node) {
        return typeof node === 'string' || !node.nodes || !node.nodes.length;
    }

    getNodeValue(node) {
        return typeof node === 'string' ? node : node.value;
    }

    filter(tree) {
        const { searchTerm } = this.props;

        return tree.filter(node => {
            const value = this.getNodeValue(node);
            const isLeaf = this.isLeaf(node);

            if (value.includes(searchTerm)) {
                return true;
            }

            if (isLeaf) {
                return false;
            }

            const subtree = this.filter(node.nodes);

            return Boolean(subtree.length);
        });
    }

    render() {
        const tree = this.createFilteredTree(
            this.state.tree,
            this.props.searchTerm
        );

        return tree.map(node => this.renderNode(node, null));
    }
}




export default withStyles(styles, { withTheme: true })(MuiTreeView)