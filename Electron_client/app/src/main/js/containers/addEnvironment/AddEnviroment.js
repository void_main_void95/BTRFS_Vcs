import React from 'react';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import  { actions as auth }  from '../../actions/auth'
import {login} from '../../RecoveryFunction'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'
import InputAdornment from '@material-ui/core/InputAdornment'
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import WatcherTree from'./WatcherTree'

const style = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: '90vh',
        justifyContent: 'center',
        margin: 0
    },
    card: {
        maxWidth: 700,
        background: grey[600]
    },
    avatar: {
        margin: '1em',
        textAlign: 'center '
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '5vh',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
    },
    input: {
        padding: 1
    },
    logo: {
        maxWidth: '50%',
        maxHeight: '50%'
    },
    button: {
        marginTop: 50,
        background: "#47738c"
    },

}

const theme = createMuiTheme({
    palette: {
        type: 'light', // Switching the dark mode on is a single property value change.
    },
});

class AddEnviroment extends React.Component {
    constructor(props){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentWillMount(){
        console.log(this.props)
    }
    handleSubmit(){
        this.props.loginRequest()
        login({
            unix_name: this.state.unix_name,
            password: this.state.password,
            token: ''
        })
    }

    handleChange = field => evt => {
        let state = {}
        state[field] = evt.target.value
        this.setState(state)
    }

    render() {

        return (
                <Grid container style={style.container}>
                    <Grid item xs={12} sm={12} style={{minWidth: 200}}>
                        <Card style={style.card} elevation={12}>
                            <CardContent>
                            <Grid container>

                                <Grid item xs={12} style={{marginBottom: 15}}>
                                    <Typography variant="display2">New home environment</Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <InputLabel>Name</InputLabel>
                                </Grid>
                                <Grid item xs={12} style={{marginBottom: 20}}>
                                    <Input
                                        id="environment_name"
                                        value={"automatic"}
                                        margin="normal"
                                        onChange={this.handleChange('environment_name')}
                                        style={style.input}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                <Typography variant="display1">Watcher</Typography>
                                </Grid>
                                <Grid item xs={6} >
                                    <InputLabel>Key</InputLabel>
                                </Grid>
                                <Grid item xs={6} >
                                    <InputLabel>File path</InputLabel>
                                </Grid>
                                <Grid item xs={6}>
                                    <Input
                                        id="watcher_key"
                                        value={this.props.password}
                                        margin="normal"
                                        style={style.input}
                                        onChange={this.handleChange('watcher_key')}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                        <WatcherTree/>
                                </Grid>
                                    <Grid item xs={12} style={{marginTop: 15}}>
                                        <InputLabel>Permission Filter</InputLabel>
                                    </Grid>
                                        <Grid item xs={12}>
                                        <Input
                                        id="watcher_permission_filter"
                                        label="Permission Filter"
                                        value={"wa"}
                                        margin="normal"
                                        style={style.input}
                                        onChange={this.handleChange('watcher_permission_filter')}
                                        disabled
                                        />
                                    </Grid>
                                <Grid item xs={12} style={{marginTop: 30}}>
                                    <Button variant="raised" color="primary" onClick={this.handleSubmit}>
                                        {this.props.loading ? <CircularProgress size={18} /> : 'Add'}
                                    </Button>
                                </Grid>

                            </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>)
    }
}


export default AddEnviroment