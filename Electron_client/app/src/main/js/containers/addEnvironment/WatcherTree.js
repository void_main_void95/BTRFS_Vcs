import React from 'react';
import { render } from 'react-dom';
import MuiTreeView from './MuiTreeView';
import grey from '@material-ui/core/colors/grey'
import {withStyles} from "@material-ui/core/styles/index";
import {connect} from "react-redux";
import {actions as watcherAction} from "../../actions/watchersAction";
import Modal from '@material-ui/core/Modal';
import {Grid, Typography, Button, TextField, CircularProgress, CardContent, Card, CardMedia, Paper} from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add';


const styles ={
    details_card: {
        maxHeight: "100%",
        maxWidth: "100%",
        marginLeft: 40,
        marginRight: 40,
        marginTop: 20,
        background: grey[600],
    },
    snapshots_card: {
        maxHeight: "100%",
        maxWidth: "100%",
        background: grey[600],
    },
    paper_grid:{
        flexGrow: 1,
        justify: "center",
        marginTop: 60
    },
    string: {
        marginLeft: 10,
        marginTop: 5,
        marginBottom: 5
    },
    add_watcher_button: {
        marginTop: 15,
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0
    },
    modal: {
        minWidth: "30%",
        maxWidth: "50%",
        maxHeight:"60%",
        overflowY: "auto",
        overflowX: "auto",
        backgroundColor: grey[600],
    },
    tree:{
        maxWidth: "100%",
        maxHeight: "100%",
        backgroundColor: grey[500],
        marginTop: 10
    },
    ok_button: {
        marginTop: 20,

    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
    }
}


class mycomponent extends React.Component {
    constructor() {
        super()

        this.state = {
            checked: [],
            indeterminate: [],
        }
    }

    getChecked = (state) => {
        this.state = state
        console.log(this.state.checked, this.state.indeterminate)
    }
    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    state = {
        open: false,
    };

    render() {
        return (
            <div>
            <Button variant="fab" color="primary" aria-label="add" onClick={this.handleOpen} style={{marginTop: 20}}>
                <AddIcon/>
            </Button>


            <Modal
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                        open={this.state.open}
                        onClose={this.handleClose}
                        style={styles.container}
                    >
                        <Card style={styles.modal}>
                            <CardContent style={styles.content}>
                            <MuiTreeView tree={this.props.tree}
                                         getChecked={this.getChecked}
                                         style={styles.tree}
                           />
                                <Button variant="contained" color="primary" onClick={this.handleClose} style={styles.ok_button}>OK</Button>
                            </CardContent>



                        </Card>


                    </Modal>

            </div>

        )
    }
}

function mapStateToProps(state){
    return {
       tree: state.WatcherReducer.tree
    }
}

function mapDispatchToProps(dispatch) {
    return({
        get_env_details: () => {dispatch(envDetails.get_env_details())},
        details_consumed: () => {dispatch(envDetails.details_consumed())}

    })
}


export default connect(mapStateToProps, mapDispatchToProps)(mycomponent);
