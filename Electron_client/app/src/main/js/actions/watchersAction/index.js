export const APPEND_WATCHER = "APPEND WATCHER"
export const POP_WATCHER = "POP_WATCHER"
export const GLOBAL_WATCHER = "GLOBAL_WATCHER"
export const LIST_COMPLETE = "LIST_COMPLETE"
export const LIST_NOT_COMPLETE = "LIST_NOT_COMPLETE"
export const GET_FILES_TREE = "GET_FILES_TREE"
export const GET_FILES_TREE_SUCCESS = "GET_FILES_TREE_SUCCESS"
export const GET_FILES_TREE_FAILED = "GET_FILES_TREE_FILED"

export const actions ={
    append_watcher: (path) => ({
        type: APPEND_WATCHER,
            path: path,

    }),
    pop_watcher: (path) => ({
        type: POP_WATCHER,
        path: path,

    }),
    global_watcher: (path) => ({
        type: GLOBAL_WATCHER,
        path: path,
    }),
    list_complete: () => ({
        type: LIST_COMPLETE,
    }),
    list_not_complete: () =>({
        type: LIST_NOT_COMPLETE,
    }),
    get_files_tree: () => ({
        type: GET_FILES_TREE,
    }),
    get_files_tree_success: (tree) =>({
        type: GET_FILES_TREE_SUCCESS,
        tree: tree

    }),
    get_files_tree_failed: (error) =>({
        type: GET_FILES_TREE_FAILED,
        error: error
    })
}
