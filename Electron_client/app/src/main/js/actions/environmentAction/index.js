export const GET_ENVS = 'GET_ENVS'
export const GET_ENVS_SUCCESS = 'GET_ENVS_SUCCESS'
export const GET_ENVS_FAILED = 'GET_ENVS_FAILED'
export const ONLY_RECOVERY = 'ONLY_RECOVERY'

export const actions ={
  get_envs: () => ({
    type: GET_ENVS,
  }),

  get_envs_success:(envs) =>({
    type: GET_ENVS_SUCCESS,
    envs: envs
  }),

  get_envs_failed: (error) => ({
    type: GET_ENVS_FAILED,
    error: error
  }),
    only_recovery: () =>({
        type: ONLY_RECOVERY,
    }),
}
