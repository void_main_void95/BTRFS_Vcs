export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const SESSION_EXPIRED = 'SESSION_EXPIRED'

export const actions = {
  loginRequest: ()  => ({
		type: LOGIN_REQUEST
	}),
  loginSuccess: (user) => ({
    type: LOGIN_SUCCESS,
    user: user
  }),
  loginFailure: (error)=>({
    type: LOGIN_FAILURE,
    error: error
  }),

	sessionExpired: (error) => ({
    type: SESSION_EXPIRED,
    error
  })
}
