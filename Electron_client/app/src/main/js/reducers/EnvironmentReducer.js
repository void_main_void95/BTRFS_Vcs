import {GET_ENVS, GET_ENVS_SUCCESS, GET_ENVS_FAILED, ONLY_RECOVERY} from '../actions/environmentAction'

const initialState = {
  requireRes : false,
  requireSuccess: false,
  requireFailed: false,
  only_recovery: false,
  envs: [{name: "NoEnv"}]
}

export const EnvironmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ENVS:
    return{
      ...state,
      requireRes: true,
    }
    case GET_ENVS_SUCCESS:
    return{
      ...state,
      requireRes: false,
      requireSuccess : true,
      envs: action.envs
    }

    case GET_ENVS_FAILED:
      return{
        ...state,
        requireRes: false,
        requireFailed : true,
    }

      case ONLY_RECOVERY:
        return{
            ...state,
            only_recovery: true,
        }



    default:
      return state

  }
}
