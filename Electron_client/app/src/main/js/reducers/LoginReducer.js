import {LOGIN_SUCCESS, LOGIN_FAILURE, LOGIN_REQUEST, SESSION_EXPIRED} from '../actions/auth'


const initialState = {
  logged: false,
  loading: false,
  fetching: false,
  user: {
    unix_name: '',
    password: '',
    token: '',
    isRoot: false,
  }
}

export const LoginReducer = (state = initialState, action) => {

  switch (action.type){
    case LOGIN_REQUEST:
      return {...state,
        logged: false,
        fetching: true,
        loading: true
      }
    case LOGIN_SUCCESS:
      return {...state,
        logged: true,
        loading: false,
        user: action.user
      }
    case LOGIN_FAILURE:
      return {...state,
        logged: false,
        loading: false,
        user: {
          email: '',
          password: '',
          token: ''
        },
        error: action.error
      }
    default:
      return state
  }
}
