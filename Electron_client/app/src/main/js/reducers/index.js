import { combineReducers } from 'redux';
import {LoginReducer} from './LoginReducer'
import {EnvironmentReducer} from './EnvironmentReducer'
import {WatchersReducer} from "./watchersReducer";

const rootReducer = combineReducers({
	LoginReducer: LoginReducer,
	EnvironmentReducer: EnvironmentReducer,
	WatcherReducer: WatchersReducer,
});

export default rootReducer;
