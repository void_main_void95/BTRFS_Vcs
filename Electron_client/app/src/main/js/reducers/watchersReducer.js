import {APPEND_WATCHER, POP_WATCHER, GLOBAL_WATCHER, LIST_COMPLETE, LIST_NOT_COMPLETE, GET_FILES_TREE, GET_FILES_TREE_FAILED, GET_FILES_TREE_SUCCESS}from '../actions/watchersAction'


const initialState = {
    list_complete: false,
    paths : [],
    global_path : "",
    global: false,
    tree: [
        {
            value: '/home/tesi',
            label: "/home/tesi",
            checked: false,
            indeterminate: false,
            nodes: [
                        {
                            value: '/Parent A',
                            label: "Parent A",
                            checked: false,
                            indeterminate: false,
                            nodes: [{ value: "/Parent A/Child A", label: 'Child A',
                                        checked: false,
                                        indeterminate: false,},
                                    { value: "/Parent A/Child B", label: 'Child B',
                                        checked: false,
                                        indeterminate: false,}],
                        },
                        {
                            label: 'Parent B',
                            value: "/Parent B",
                            checked: false,
                            indeterminate: false,
                            nodes: [
                                {
                                    value: '/Parent B/Child C',
                                    label: 'Child C',
                                    checked: false,
                                    indeterminate: false,

                                },
                                {
                                    value: "/Parent B/Parent C",
                                    label: 'Parent C',
                                    checked: false,
                                    indeterminate: false,
                                    nodes: [
                                        { label: 'Child D',
                                          value:"/Parent B/Parent C/Child D",
                                            checked: false,
                                            indeterminate: false,
                                           },
                                        { label: 'Child E',
                                            value:"/Parent B/Parent C/Child E",
                                            checked: false,
                                            indeterminate: false,
                                           },
                                        { label: 'Child F',
                                            value:"/Parent B/Parent C/Child F",
                                            checked: false,
                                            indeterminate: false,
                                         },
                                        {
                                            label: 'Parent D',
                                            value: "/Parent B/Parent C/Parent D",
                                            checked: false,
                                            indeterminate: false,
                                            nodes: [
                                                {
                                                    label: 'Child G',
                                                    value: "/Parent B/Parent C/Parent D/Child G",
                                                    checked: false,
                                                    indeterminate: false,
                                                },
                                            ],
                                        },
                                    ],
                                },
                            ],
                        },
                            ]
        }
    ],
    requireSuccess: false,
    required: false,
    error: "",
    }

export const WatchersReducer = (state = initialState, action) => {

    switch (action.type) {
        case APPEND_WATCHER:
            return {
                ...state,
                paths: state.paths.push(action.path)
            }
        case POP_WATCHER:
            if (state.paths.indexOf(action.path) > -1) {
                return {
                    ...state,
                    paths: state.paths.splice(state.paths.indexOf(action.path), 1)
                }
            }
            break

        case GLOBAL_WATCHER:
            return {
                ...state,
                global: true,
                global_path: action.path
            }

        case LIST_COMPLETE:
            return {
                ...state,
                list_complete: true
            }

        case LIST_NOT_COMPLETE:
            return {
                ...state,
                list_complete: false
            }

        case GET_FILES_TREE:
            return{
                ...state,
                required: true,
            }

        case GET_FILES_TREE_SUCCESS:
            return{
                ...state,
                requireSuccess: true,
                required: false,
                tree: action.tree
            }
        default:
            return state
    }
}
