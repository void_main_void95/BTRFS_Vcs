import React, { Component } from 'react';
import Main from './components/main';
import store from './store'
import {Redirect, Switch} from 'react-router-dom'
import LoginComponent from './containers/login/LoginWindow'
import UserWindow from './containers/userWindow/userWindow'


import {
  HashRouter as Router,
  Route,
} from 'react-router-dom';


export default class extends Component {
	render() {
		return (

			<Router>
        <Switch>
					<Route exact path="/" component={LoginComponent}/>
          <Route path="/main2" component={UserWindow}/>
          <Redirect from="/" to="/main2"/>
        </Switch>
			</Router>

		)

	}
}
