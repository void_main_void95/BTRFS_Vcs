import {ipcRenderer} from 'electron'
import  { actions as auth }  from '../actions/auth'
import {actions as envAction} from '../actions/environmentAction'
import store from '../store'

 export const login = ({unix_name, password, token}) =>{

  ipcRenderer.send("auth", unix_name, password)
     let isRoot = false
     if (unix_name === "recovery"){
        isRoot = true
     }

  ipcRenderer.on("set:token", function(e, token){
    localStorage.setItem("current-token", token)
    store.dispatch(auth.loginSuccess({unix_name, password, token, isRoot}))
  })
  ipcRenderer.on('auth:failed', function(e, reason){
    store.dispatch(auth.loginFailure(reason))
  })
}



export const get_environments = (token) => {
  ipcRenderer.send('get:envs', token)

  ipcRenderer.on('get:env:success', function(e, envs){
    store.dispatch(envAction.get_envs_success(envs))
      if (envs.length === 1){
          store.dispatch(envAction.only_recovery())
      }
  })

  ipcRenderer.on('get:env:failed', function(e, reason){
    if (reason === "token_expired"){
      store.dispatch(auth.sessionExpired(reason))
      }

  else{
    store.dispatch(envAction.get_envs_failed(reason))
  }
  })

}

