var net = require('net')

var port = 6660
var host = "192.168.122.147"
var apiVersion ="v1.0.1"

export const authenticate = function(username, password, callback){
  var client = new net.Socket()
  var buffer = ''
  client.connect(port, host, function(){
    var message = {
      'command' : 'auth',
      'payload': {
        'username' : username,
        'password': password
      }
    }
    message = JSON.stringify(message) + "\n";
    client.write(message)
  })

  client.on('data', function(data){
    data = data.toString()
    if (data.charAt(data.lenght -1) != "\n" &&
       data.charAt(data.lenght -2) != "}" ){
      buffer += data
    }
    else{
      client.end()
      client.destroy()
    }
  })

  client.on('error', function(err){
    console.log(err)
  })

  client.on('close', function(){
    buffer = buffer.slice(0,-1)
    data = JSON.parse(buffer);
    callback(data)
    console.log('closed')
  })

}

 /*
module.exports = {

apiVersion :  apiVersion,


authenticate : function(username, password, callback){
            var client = new net.Socket()
            var buffer = ''
            client.connect(port, host, function(){
              var message = {
                'command' : 'auth',
                'payload': {
                  'username' : username,
                  'password': password
                }
              }
              message = JSON.stringify(message) + "\n";
              client.write(message)
            })

            client.on('data', function(data){
              data = data.toString()
              if (data.charAt(data.lenght -1) != "\n" &&
                 data.charAt(data.lenght -2) != "}" ){
                buffer += data
              }
              else{
                client.end()
                client.destroy()
              }
            })

            client.on('error', function(err){
              console.log(err)
            })

            client.on('close', function(){
              buffer = buffer.slice(0,-1)
              data = JSON.parse(buffer);
              callback(data)
              console.log('closed')
            })
        },

get_snapshot : function(token, callback){
      var client = new net.Socket()
      var buffer = ''
        client.connect(port, host, function(){
          var message = {
            'command': 'getSnapshot',
            'token': token
          }
          message = JSON.stringify(message) + "\n"
          client.write(message)
        })

        client.on('data', function(data){
          data = data.toString()
          if (data.charAt(data.lenght -1) != "\n" &&
             data.charAt(data.lenght -2) != "}" ){
            buffer += data
          }
          else{
            client.end()
            client.destroy()
          }
        })

        client.on('error', function(err){
          console.log(err)
        })

        client.on('close', function(){
          buffer = buffer.slice(0,-1)
          data = JSON.parse(buffer);
          callback(data)
          console.log('closed')
        })

      },

make_snapshot : function(name, owner, token, callback){
        var client = new net.Socket()
        var buffer = ''
        client.connect(port, host, function(){
          var message = {
            'command': 'makeSnapshot',
            'token': token,
            'payload': {
              'name': name
            }
          }
          message = JSON.stringify(message) + "\n"
          client.write(message)
        })

        client.on('data', function(data){
          data = data.toString()
          if (data.charAt(data.lenght -1) != "\n" &&
             data.charAt(data.lenght -2) != "}" ){
            buffer += data
          }
          else{
            client.end()
            client.destroy()
          }
        })

        client.on('error', function(err){
          console.log(err)
        })

        client.on('close', function(){
          buffer = buffer.slice(0,-1)
          data = JSON.parse(buffer);
          callback(data)
          console.log('closed')
        })
      },

get_log : function(name, token, callback){
          var client = new net.Socket()
          var buffer = ''
          var buffer = ''
          client.connect(port, host, function(){
            var message = {
              'command': 'getLog',
              'token': token,
              'payload': {
                'log_name': name
              }
            }
            message = JSON.stringify(message) + "\n"
            client.write(message)
          })

          client.on('data', function(data){
            data = data.toString()
            if (data.charAt(data.lenght -1) != "\n" &&
               data.charAt(data.lenght -2) != "}" ){
              buffer += data
            }
            else{
              client.end()
              client.destroy()
            }
          })

          client.on('error', function(err){
            console.log(err)
          })

          client.on('close', function(){
            buffer = buffer.slice(0,-1)
            data = JSON.parse(buffer);
            callback(data)
            console.log('closed')
          })
        },

delete_snapshot : function(fs_id, token, callback){
            var client = new net.Socket()
            var buffer = ''
            client.connect(port, host, function(){
              var message = {
                'command': 'deleteSnapshot',
                'token': token,
                'payload': {
                  'fs_id': fs_id
                }
              }
              message = JSON.stringify(message) + "\n"
              client.write(message)
            })

            client.on('data', function(data){
              data = data.toString()
              if (data.charAt(data.lenght -1) != "\n" &&
                 data.charAt(data.lenght -2) != "}" ){
                buffer += data
              }
              else{
                client.end()
                client.destroy()
              }
            })

            client.on('error', function(err){
              console.log(err)
            })

            client.on('close', function(){
              data = JSON.parse(buffer);
              callback(data)
              console.log('closed')
            })
          },

get_config : function(volume, token, callback){
              var client = new net.Socket()
              var buffer = ''
              client.connect(port, host, function(){
                var message = {
                  'command': 'getConfig',
                  'token': token,
                  'payload': {
                    'volume': volume
                  }
                }
                message = JSON.stringify(message) + "\n"
                client.write(message)
              })

              client.on('data', function(data){
                data = data.toString()
                if (data.charAt(data.lenght -1) != "\n" &&
                   data.charAt(data.lenght -2) != "}" ){
                  buffer += data
                }
                else{
                  client.end()
                  client.destroy()
                }
              })

              client.on('error', function(err){
                console.log(err)
              })

              client.on('close', function(){
                buffer = buffer.slice(0,-1)
                data = JSON.parse(buffer);
                callback(data)
                console.log('closed')
              })
            },

plan_restore : function(fs_id, token, callback){
                  var client = new net.Socket()
                  var buffer = ''
                  client.connect(port, host, function(){
                    var message = {
                      'command': 'planRestore',
                      'token': token,
                      'payload': {
                        'fs_id': fs_id
                      }
                    }
                    message = JSON.stringify(message) + "\n"
                    client.write(message)
                  })

                  client.on('data', function(data){
                    data = data.toString()
                    if (data.charAt(data.lenght -1) != "\n" &&
                       data.charAt(data.lenght -2) != "}" ){
                      buffer += data
                    }
                    else{
                      client.end()
                      client.destroy()
                    }
                  })

                  client.on('error', function(err){
                    console.log(err)
                  })

                  client.on('close', function(){
                    buffer = buffer.slice(0,-1)
                    data = JSON.parse(buffer);
                    callback(data)
                    console.log('closed')
                  })
                },

get_report : function(fs_id, token, callback){
            var client = new net.Socket()
            var buffer = ''
            client.connect(port, host, function(){
              var message = {
                'command': 'getReport',
                'token': token,
                'payload': {
                  'fs_id': fs_id
                }
              }
              message = JSON.stringify(message) + "\n"
              client.write(message)
            })

            client.on('data', function(data){
              data = data.toString()
              if (data.charAt(data.lenght -1) != "\n" &&
                 data.charAt(data.lenght -2) != "}" ){
                buffer += data
              }
              else{
                client.end()
                client.destroy()
              }
            })

            client.on('error', function(err){
              console.log(err)
            })

            client.on('close', function(){
              buffer = buffer.slice(0,-1)
              data = JSON.parse(buffer);
              callback(data)
              console.log('closed')
            })
          },

insert_env_watcher : function(env_id, key, file, permission_filter, token, callback){
              var client = new net.Socket()
              var buffer = ''
              client.connect(port, host, function(){
                var message = {
                  'command': 'insertEnvWatcher',
                  'token': token,
                  'payload': {
                    'env_id': env_id,
                    'watcher': {
                    'key': key,
                    'file': file,
                    'permission_filter':permission_filter
                    }
                  }
                }
                message = JSON.stringify(message) + "\n"
                client.write(message)
              })

              client.on('data', function(data){
                data = data.toString()
                if (data.charAt(data.lenght -1) != "\n" &&
                   data.charAt(data.lenght -2) != "}" ){
                  buffer += data
                }
                else{
                  client.end()
                  client.destroy()
                }
              })

              client.on('error', function(err){
                console.log(err)
              })

              client.on('close', function(){
                buffer = buffer.slice(0,-1)
                data = JSON.parse(buffer);
                callback(data)
                console.log('closed')
              })
            },

  delete_env_watcher : function(env_id, watcher, token, callback){
              var client = new net.Socket()
              var buffer = ''
              client.connect(port, host, function(){
                var message = {
                  'command': 'deleteEnvWatcher',
                  'token': token,
                  'payload': {
                    'env_id': env_id,
                    'watcher': watcher
                    }
                  }
                message = JSON.stringify(message) + "\n"
                client.write(message)
              })

              client.on('data', function(data){
                data = data.toString()
                if (data.charAt(data.lenght -1) != "\n" &&
                   data.charAt(data.lenght -2) != "}" ){
                  buffer += data
                }
                else{
                  client.end()
                  client.destroy()
                }
              })

              client.on('error', function(err){
                console.log(err)
              })

              client.on('close', function(){
                buffer = buffer.slice(0,-1)
                data = JSON.parse(buffer);
                callback(data)
                console.log('closed')
              })
            },

create_environment : function(env_name, key, file, permission_filter, token, callback){
              var client = new net.Socket()
              var buffer = ''
              client.connect(port, host, function(){
                var message = {
                  'command': 'createEnvironment',
                  'token': token,
                  'payload': {
                    'env_name': env_name,
                    'watcher': {
                        'key': key,
                        'file': file,
                        'permission_filter':permission_filter
                    }
                  }
                }
                message = JSON.stringify(message) + "\n"
                client.write(message)
              })

              client.on('data', function(data){
                data = data.toString()
                if (data.charAt(data.lenght -1) != "\n" &&
                   data.charAt(data.lenght -2) != "}" ){
                  buffer += data
                }
                else{
                  client.end()
                  client.destroy()
                }
              })

              client.on('error', function(err){
                console.log(err)
              })

              client.on('close', function(){
                buffer = buffer.slice(0,-1)
                data = JSON.parse(buffer);
                callback(data)
                console.log('closed')
              })
            },

delete_environment : function(env_id, token, callback){
              var client = new net.Socket()
              var buffer = ''
              client.connect(port, host, function(){
                var message = {
                  'command': 'deleteEnvironment',
                  'token': token,
                  'payload': {
                    'env_id': env_id
                  }
                }
                message = JSON.stringify(message) + "\n"
                client.write(message)
              })

              client.on('data', function(data){
                data = data.toString()
                if (data.charAt(data.lenght -1) != "\n" &&
                   data.charAt(data.lenght -2) != "}" ){
                  buffer += data
                }
                else{
                  client.end()
                  client.destroy()
                }
              })

              client.on('error', function(err){
                console.log(err)
              })

              client.on('close', function(){
                buffer = buffer.slice(0,-1)
                data = JSON.parse(buffer);
                callback(data)
                console.log('closed')
              })
            },

get_environments : function(token, callback){
                var client = new net.Socket()
                var buffer = ''
                client.connect(port, host, function(){
                  var message = {
                    'command': 'getEnvironments',
                    'token': token
                  }
                  message = JSON.stringify(message) + "\n"
                  client.write(message)
                })

                client.on('data', function(data){
                  data = data.toString()
                  if (data.charAt(data.lenght -1) != "\n" &&
                     data.charAt(data.lenght -2) != "}" ){
                    buffer += data
                  }
                  else{
                    client.end()
                    client.destroy()
                  }
                })

                client.on('error', function(err){
                  console.log(err)
                })

                client.on('close', function(){
                  buffer = buffer.slice(0,-1)
                  data = JSON.parse(buffer);
                  callback(data)
                  console.log('closed')
                })
              },

environment_details : function(env_id, token, callback){
                  var client = new net.Socket()
                  var buffer = ''
                  client.connect(port, host, function(){
                    var message = {
                      'command': 'envDetail',
                      'token': token,
                      'payload':{
                        'env_id' : env_id
                      }

                    }
                    message = JSON.stringify(message) + "\n"
                    client.write(message)
                  })

                  client.on('data', function(data){
                    data = data.toString()
                    if (data.charAt(data.lenght -1) != "\n" &&
                       data.charAt(data.lenght -2) != "}" ){
                      buffer += data
                    }
                    else{
                      client.end()
                      client.destroy()
                    }
                  })

                  client.on('error', function(err){
                    console.log(err)
                  })

                  client.on('close', function(){
                    buffer = buffer.slice(0,-1)
                    data = JSON.parse(buffer);
                    callback(data)
                    console.log('closed')
                  })
                },




*/
