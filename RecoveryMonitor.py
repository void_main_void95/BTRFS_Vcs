#!/usr/bin/python3
import lib.demon as demon
import signal
import lib.Logger as libLogger
from lib.exception import *
import datetime
import sys
import os
import threading
import lib.DbInterface as libDatabase
import lib.UserInterface as userInterface
import time
import socket

def term_handler(signal, frame):
    try:
        print("Stopping daemon")
        recovery_daemon.stop()
        print("Waiting daemon exiting")
        recovery_daemon.join()
        print("Demon exited")
        print("Stoppin userInterface")
        user_interface.stop()
        print("Waiting userInterface exiting")
        user_interface.join()
        print("userInterface exited")
        logger.write_monitor_info("Recovery Monitor Stop")
        logger.close()
        print("Recovery Monitor Stop")
    except RuntimeError as e:
        print("error")


lock = threading.Lock()
local_path = os.path.dirname(os.path.abspath(__file__))
lock.acquire()
database = libDatabase.DbInteface(local_path + "/database/recovery.db")
database_path = database.get_path("DatabaseFolder")
log_path = database.get_path("LogsFolder")
lock.release()
database.close()
logger = libLogger.MonitorLogger(log_path)
recovery_daemon = demon.Daemon(database_path, lock)
recovery_daemon.start()
logger.write_monitor_info("recovery_daemon started")
user_interface = userInterface.UserInterface(database_path, lock)
max_retry = 5
while max_retry != 0:
    try:
        user_interface.start()
        logger.write_monitor_info("user_interface started")
        max_retry = 0
    except socket.error as e:
        logger.write_monitor_error("Starting user_interface", e.args[0])
        logger.write_monitor_info("Next attempt in 20 seconds")
        max_retry -= 1
        time.sleep(20)

logger.write_monitor_info("Recovery monitor start")
print("Recovery monitor start\n")


signal.signal(signal.SIGTERM, term_handler)
signal.signal(signal.SIGINT, term_handler)
