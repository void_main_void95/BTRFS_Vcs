import json
import os
import datetime
import socket as sck
import threading
import sys
import lib.DbInterface as libDatabase
import lib.BtrfsRecovery as libRecovery
from lib.exception import *
import jwt
import datetime
import lib.Logger as libLogger
import hashlib


class UserInterface(threading.Thread):

    def __init__(self, db_path, lock):
        threading.Thread.__init__(self)
        self.address = "192.168.122.56"
        self.port = 6660
        self.lock = lock
        self.db_path = db_path
        self.socket = sck.socket(sck.AF_INET, sck.SOCK_STREAM)
        self.socket.settimeout(10.0)
        self.JWT_SECRET = "secret"
        self.JWT_ALGORITHM = "HS256"
        self.JWT_EXP_DELTA_SECONDS = 3600
        self.closeEvent = threading.Event()


    def run(self):
        self.lock.acquire()
        self.db = libDatabase.DbInteface(self.db_path + "/recovery.db")
        self.logger = libLogger.UserClientLogger()
        self.recovery = libRecovery.BtrfsRecovery(thread_owner="user_interface")
        self.lock.release()
        try:
            self.socket.bind((self.address, self.port))
            self.logger.write_write_info("Socket binded at: " + self.address + ":" + str(self.port))
        except sck.error as e:
            self.logger.write_user_client_error("In binding socket" , str(e))
            self.closeEvent.set()
            self.db.close()
            raise e

        print("Server now listening on 6660")
        self.socket.listen(3)
        while not self.closeEvent.isSet():
            try:
                conn, address = self.socket.accept()
                fd_conn = conn.makefile("rw")
                print("Got connection from: ", address)
                data = fd_conn.readline()
                fd_conn.flush()
                data.replace('\n', '')
                message = json.loads(data)
                self.handle_request(fd_conn, conn, message)
                continue
            except Exception as e:
                continue

        try:
            self.socket.shutdown(sck.SHUT_RDWR)
        except Exception as e:
            print("Error: " + str(e))
        self.db.close()

    def stop(self):
        self.closeEvent.set()


    def handle_request(self, fd_connection, connection, message):
        if message["command"] == "auth":
            response = self.authenticate_user(message)
        else:
            response = self.login_required(message)
        fd_connection.write(json.dumps(response) + "\n")
        fd_connection.flush()
        fd_connection.close()
        connection.close()
        print("Data sent")

    def login_required(self,message):
        response = dict()
        try:
            current_user = self.decode_token(jwt_token=message["token"])
            print(message)
            if message["command"] == "getConfig":
                self.lock.acquire()
                response = self.get_config(volume=message["payload"]["volume"])
                self.lock.release()
            elif message["command"] == "getSnapshot":
                self.lock.acquire()
                response = self.get_snapshot(current_user=current_user)
                self.lock.release()
            elif message["command"] == "makeSnapshot":
                self.lock.acquire()
                response = self.make_snapshot(owner=current_user, name=message["payload"]["name"])
                self.lock.release()
            elif message["command"] == "deleteSnapshot":
                self.lock.acquire()
                response = self.delete_snapshot(snapshot_id=message["payload"]["fs_id"], owner=current_user)
                self.lock.release()
            elif message["command"] == "getLog":
                self.lock.acquire()
                response = self.get_log(log_name=message["payload"]["log_name"])
                self.lock.release()
            elif message["command"] == "planRestore":
                self.lock.acquire()
                response = self.plan_recovery(owner=current_user, snapshot_id=message["payload"]["fs_id"])
                self.lock.release()
            elif message["command"] == "getReport":
                self.lock.acquire()
                response = self.get_report(snapshot_id=message["payload"]["fs_id"])
                self.lock.release()
            elif message["command"] == "insertEnvWatcher":
                self.lock.acquire()
                response = self.insert_env_watcher(env_id= message["payload"]["env_id"] ,watcher=message["payload"]["watcher"], owner=current_user)
                self.lock.release()
            elif message["command"] == "deleteEnvWatcher":
                self.lock.acquire()
                response = self.delete_env_watcher(env_id=message["payload"]["env_id"], watcher=message["payload"]["watcher"], owner=current_user)
            elif message["command"] == "createEnvironment":
                self.lock.acquire()
                response = self.create_home_environment(env_name=message["payload"]["env_name"], watcher=message["payload"]["watcher"], owner=current_user)
                self.lock.release()
            elif message["command"] == "deleteEnvironment":
                self.lock.acquire()
                response = self.delete_home_environment(env_id=message["payload"]["env_id"], owner=current_user)
                self.lock.release()
            elif message["command"] == "getEnvironments":
                self.lock.acquire()
                response = self.get_environments()
                self.lock.release()
            elif message["command"] == "envDetail":
                self.lock.acquire()
                response = self.environment_details(env_id=message["payload"]["env_id"], owner=current_user)
                self.lock.release()
            else:
                response["command"] = "methodNotAllowed"


        except (jwt.DecodeError, jwt.ExpiredSignatureError) as e:
            response["command"] = "invalidToken"
        finally:
            return response

#evironments methods
    def create_home_environment(self, env_name, watcher, owner):
        response = dict()
        try:
            creation_date = datetime.datetime.now()
            if env_name == "automatic":
                env_name = "home_" + owner + "_environment_" + creation_date.strftime("%Y-%m-%d/%H:%M:%S")
            env_id = hashlib.md5(env_name.encode()).hexdigest()
            self.db.insert_environment(env_id=env_id, name=env_name,
                                       creation_date=creation_date.strftime("%Y-%m-%d %H:%M:%S"), owner=owner, status="new")
            name = "auto_" + creation_date.strftime("%Y-%m-%d/%H:%M:%S")
            snapshot_volume = self.db.get_config("SnapshotVolume")
            recovery_volume = self.db.get_config("RecoveryVolume")
            id = hashlib.sha512(name.encode()).hexdigest()
            new_snapshot = self.recovery.make_snapshot(id=id, snapshot_folder=snapshot_volume, recovery_volume=recovery_volume, user=owner)
            self.db.insert_snapshot(fs_id=new_snapshot["id"], name=name, creation_date=creation_date.strftime("%Y-%m-%d %H:%M:%S"),
                                    owner=owner, type="env", volid=new_snapshot["volid"])
            self.db.insert_env_snapshot(env_id=env_id, snap_id=new_snapshot["id"])
            self.db.insert_watcher(key=watcher["key"], file=watcher["file"], permission_filter=watcher["permission_filter"], owner=owner, type="env")
            self.db.insert_env_watcher(env_id=env_id, watcher_key=watcher["key"])
            self.db.update_recovery_plan(user=owner, snap_id=new_snapshot["id"])
            response["command"] = "environmentCreated"

        except SnapshotException as e:
            self.logger.write_user_client_error("Creating "+ owner+  " environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "snapshotException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "createFailed"

        except VolidException as e:
            self.logger.write_user_client_error("Creating recovery environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "volidException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "createFailed"

        except MountErrorException as e:
            self.logger.write_user_client_error("Creating recovery environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "mountException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "createFailed"

        except UmountErrorException as e:
            self.logger.write_user_client_error("Creating recovery environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "umountException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "createFailed"

        except Exception as e:
            print("errrore" + str(e))

        finally:
            return response

    def delete_home_environment(self, env_id, owner):
        response = dict()
        env = self.db.environment_detail(env_id)
        if env != "NoValue":
            if env["owner"] == owner:
                snapshot_volume = self.db.get_config("SnapshotVolume")
                recovery_volume = self.db.get_config("RecoveryVolume")
                for snap in env["snapshots"]:
                    try:
                        print(snap)
                        self.recovery.delete_snapshot(snapshot_id=snap["fs_id"], snapshot_volume=snapshot_volume, recovery_volume=recovery_volume)
                        self.db.delete_env_snapshot(env["env_id"], snap["fs_id"])
                        self.db.delete_snapshot(snap["fs_id"])
                        response["command"] = "deleteSuccess"

                    except SnapshotException as e:
                        self.logger.write_user_client_error("Creating " + owner + " environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "snapshotException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFailed"

                    except VolidException as e:
                        self.logger.write_user_client_error("Creating recovery environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "volidException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFailed"

                    except MountErrorException as e:
                        self.logger.write_user_client_error("Creating recovery environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "mountException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFailed"

                    except UmountErrorException as e:
                        self.logger.write_user_client_error("Creating recovery environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "umountException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFailed"

                for watcher in env["watchers"]:
                    print(watcher)
                    self.db.delete_env_watcher(env["env_id"], watcher_key=watcher["key"])
                    self.db.delete_watcher(key=watcher["key"])
                self.db.delete_environment(env["env_id"])
                response["command"] = "deleteSuccess"
            else:
                response["command"] = "deleteFailed"
                response["reason"] = "unauthorized"
        else:
            response["command"] = "deleteFailed"
            response["reason"] = "NoValue"
        return response

    def get_environments(self):
        response = dict()
        environments = self.db.get_env_ids()
        if environments != "NoValue":
            json_result = list()
            for env in environments:
                details = self.db.environment_detail(env)
                json_result.append(details)
            response["command"] = "getSuccess"
            response["payload"] = json_result
        else:
            response["command"] = "getFailed"
            response["reason"] = "NoValue"
        return response

    def environment_details(self, env_id, owner):
        response = dict()
        details = self.db.environment_detail(env_id)
        if details != "NoValue":
            if details["owner"] == owner:
                response["command"] = "getSuccess"
                response["payload"] = details
            else:
                response["command"] = "getFailed"
                response["reason"] = "unauthorized"
        else:
            response["command"] = "getFailed"
            response["reason"] = "NoEnv"

        return response


#watchers methods
    def delete_env_watcher(self, env_id, watcher, owner):
        response = dict()
        details = self.db.environment_detail(env_id=env_id)
        if details["owner"] == owner:
            if watcher in details["watchers"]:
                self.db.delete_env_watcher(env_id=env_id, watcher_key=watcher["key"])
                self.db.delete_watcher(key = watcher["key"])
                response["command"] = "deleteSuccess"
            else:
                response["command"] = "deleteFailed"
                response["reason"] = "notEnvWatcher"
        else:
            response["command"] = "deleteFailed"
            response["reason"] = "unauthorized"

        return response

    def insert_env_watcher(self, env_id, watcher, owner):
        response = dict()
        if "/home" in watcher["file"]:
            self.db.insert_watcher(key=watcher["key"], file=watcher["file"], permission_filter=watcher["permission_filter"], owner=owner, type=watcher["type"])
            self.db.insert_env_watcher(env_id=env_id, watcher_key=watcher["key"])
            response["command"] = "insertSuccess"
        else:
            response["command"] = "insertFailed"
            response["reason"] = "unauthorized"
        return response

#report methods
    def get_report(self, snapshot_id):
        response = dict()
        if snapshot_id == "all":
            report = self.db.get_report()
        else:
            report = self.db.get_report(snapshot_id)
        if report == "NoValue":
            response["command"] = "getFailed"
            response["reason"] = "Novalue"
        else:
            file_report = self.load_file(self.db_path + "/" + report["id"], type="report")
            response["command"] = "getSuccess"
            response["payload"] = file_report
        return response

#plan methods
    def plan_recovery(self, owner, snapshot_id):
        response = dict()
        env_id = self.db.get_env_from_snap_id(snap_id=snapshot_id)
        if env_id != "NoValue":
            if self.db.update_recovery_plan(user=owner, snap_id=snapshot_id) == 0:
                response["command"] = "planSuccess"
                response["payload"] = details
            else:
                response["command"] = "planFailed"
                response["reason"] = "error"
        else:
                response["command"] = "planFailed"
                response["reason"] = "NoValue"
        return response

#log methods
    def get_log(self, log_name):
        response = dict()
        log_path = self.db.get_path("LogsFolder")
        if log_name == "daemon.log":
            try:
                log = self.load_file(log_path + "/daemon.log")
                response["command"] = "getSuccess"
                response["payload"] = log
            except IOError as e:
                response["command"] = "getFailed"
                response["reason"] = "No daemon.log found!"

        elif log_name == "userInterface.log":
            try:
                log = self.load_file(log_path + "/userInterface.log")
                response["command"] = "getSuccess"
                response["payload"] = log
            except IOError as e:
                response["command"] = "getFailed"
                response["reason"] = "No userInterface.log found!"

        elif log_name == "monitor.log":
            try:
                log = self.load_file(log_path + "/monitor.log")
                response["command"] = "getSuccess"
                response["payload"] = log

            except IOError as e:
                response["command"] = "getFailed"
                response["reason"] = "No monitor.log found!"

        else:
            response["command"] = "getFailed"
            response["reason"] = "No log found!"
        return response

#snapshot methods
    def delete_snapshot(self, snapshot_id, owner, env_id = False):
        response = dict()
        details = self.db.get_snapshots_detail(snapshot_id)
        if details != "NoValue":
            if details["owner"] == owner:
                if details["type"] == "user":
                    try:
                        snapshot_volume = self.db.get_config("SnapshotVolume")
                        recovery_volume = self.db.get_config("RecoveryVolume")
                        self.recovery.delete_snapshot(snapshot_id=snapshot_id, snapshot_volume=snapshot_volume, recovery_volume=recovery_volume)
                        self.db.delete_snapshot(snapshot_id=snapshot_id)
                        response["command"] = "deleteSuccess"
                        response["payload"] = details

                    except SnapshotException as e:
                        self.logger.write_user_client_error("Creating " + owner + " snapshot", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "snapshotException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFail"

                    except VolidException as e:
                        self.logger.write_user_client_error("Creating " + owner + " snapshot", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "volidException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFail"

                    except MountErrorException as e:
                        self.logger.write_user_client_error("Creating " + owner + " snapshot", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "mountException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFail"

                    except UmountErrorException as e:
                        self.logger.write_user_client_error("Creating " + owner + " snapshot", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                            response["reason"] = "umountException"
                        except UmountErrorException as e:
                            self.logger.write_user_client_error("Emergency Umount", e.get_error())
                            response["reason"] = "emergencyUmountError"
                        finally:
                            response["command"] = "deleteFail"
                else:
                    response["command"] = "deleteFailed"
                    response["reason"] = "notUserSnapshot"
            else:
                response["command"] = "deleteFailed"
                response["payload"] = "unauthorized"
        else:
            response["command"] = "deleteFailed"
            response["payload"] = "NoValue"
        return response

    def make_snapshot(self, name, owner):
        response = dict()
        try:
            snapshot_volume = self.db.get_config("SnapshotVolume")
            recovery_volume = self.db.get_config("RecoveryVolume")
            current_time = datetime.datetime.now()
            id = hashlib.sha512(name.encode()).hexdigest()
            new_snapshot = self.recovery.make_snapshot(id=id, snapshot_folder=snapshot_volume, recovery_volume=recovery_volume, user=owner)
            self.db.insert_snapshot(fs_id=new_snapshot[id], name=name, creation_date=current_time.strftime("%Y-%m-%d %H:%M:%S"),
                                    owner=owner, volid=new_snapshot["volid"], type="user")
            response["command"] = "makeSuccess"

        except SnapshotException as e:
            self.logger.write_user_client_error("Creating " + owner + " snapshot", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "snapshotException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "makeFail"

        except VolidException as e:
            self.logger.write_user_client_error("Deleting " + owner + " snapshot", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "volidException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "makeFail"

        except MountErrorException as e:
            self.logger.write_user_client_error("Deleting " + owner + " snapshot", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "mountException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "makeFail"

        except UmountErrorException as e:
            self.logger.write_user_client_error("Deleting " + owner + " snapshot", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
                response["reason"] = "umountException"
            except UmountErrorException as e:
                self.logger.write_user_client_error("Emergency Umount", e.get_error())
                response["reason"] = "emergencyUmountError"
            finally:
                response["command"] = "makeFail"

        return response

    def get_snapshot(self, current_user):
        response = dict()
        if current_user  == "all":
            snapshots = self.db.get_snapshot_list()
        else:
            snapshots = self.db.get_snapshot_list(current_user)

        if snapshots == "NoValue":
            response["command"] = "getFailed"
            response["payload"] = "NoValue"
        else:
            response["command"] = "getSuccess"
            response["payload"] = snapshots
        return response

#config methods
    def get_config(self, volume):
        response = dict()
        current_config = self.db.get_config(volume)
        if current_config == "NoValue":
            response["command"] = "getFailed"
            response["payload"] = "NoValue"
        else:
            response["command"] = "getSuccess"
            response["payload"] = current_config
        return response

#file methods
    def load_file(self, file_path, type="log"):
        try:
            log_fd = open(file_path, "r")
            if type == "log":
                log = log_fd.read()
            else:
                log = json.load(log_fd)
            log_fd.close()
            return log
        except IOError as e:
            raise e

    def get_all_path(self, folder):
        all_file = os.listdir(folder)
        absolute_paths = list()
        absolute_paths.append(folder)
        response = dict()
        response["commang"] = "getSuccess"
        for file in all_file:
            path = folder + "/" + file
            absolute_paths.append(path)
        response["payload"] = absolute_paths
        return response

#security methods
    def generate_token(self, username):
        payload = {
            "unix_name" : username,
            "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds= self.JWT_EXP_DELTA_SECONDS)
        }
        jwt_token = jwt.encode(payload, self.JWT_SECRET, self.JWT_ALGORITHM)
        return jwt_token.decode('utf-8')

    def decode_token(self, jwt_token):
        try:
            payload = jwt.decode(jwt_token, self.JWT_SECRET, leeway=datetime.timedelta(seconds=self.JWT_EXP_DELTA_SECONDS), algorithms=self.JWT_ALGORITHM)
            return  payload["unix_name"]
        except (jwt.DecodeError, jwt.ExpiredSignatureError) as e:
            raise e

    def authenticate_user(self, message):
        response = dict()
        self.lock.acquire()
        result = self.db.authenticate(message["payload"]["username"], message["payload"]["password"])
        self.lock.release()
        if result:
            response["command"] = "setToken"
            response["payload"] = dict()
            response["payload"]["token"] = self.generate_token(message["payload"]["username"])
        else:
            response["command"] = "unauthorized"
        return response

