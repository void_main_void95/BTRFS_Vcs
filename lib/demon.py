import datetime
import threading
import lib.AuditWrapper as audit
from lib.exception import *
import sys
import json
import os
import hashlib
import lib.DbInterface as libDatabase
import lib.notification as Notification
import lib.BtrfsRecovery as libRecovery
import lib.Logger as libLogger

class Daemon(threading.Thread):
    def __init__(self, db_path, lock_object):
            threading.Thread.__init__(self)
            self.lock = lock_object
            self.db_path = db_path
            self.last_changes = dict()
            self.time_sleep = 30
            self.control_point = dict()
            self.require_sanity_check = False
            self.current_env_id = 0
            self.closeEvent = threading.Event()
            #self.notify = Notification.Notification()

    def run(self):
        self.lock.acquire()
        self.db = libDatabase.DbInteface(self.db_path + "/recovery.db")
        self.recovery = libRecovery.BtrfsRecovery("daemon")
        logs_path = self.db.get_path("LogsFolder")
        self.logger = libLogger.DaemonLogger(logs_path)
        boot_time = datetime.datetime.now()

        if self.create_environment(current_time=boot_time, owner="recovery") == 0:
            print("Enviroment Created")
            return_val = self.clean_last_environment(boot_time)
            if return_val == 0:
                print("Enviroment Cleaned")
            elif return_val == 2:
                print("No environment to clean")
            else:
                print("Error")
            self.control_point[self.current_env_id] = dict()
            self.logger.write_demon_info("Recovery Daemon start ")
        else:
            print("Error in create")

        self.lock.release()


        if self.require_sanity_check:
            self.lock.acquire()
            all_snapshots = self.db.get_snapshot_list("all")
            recovery_volume = self.db.get_config("RecoveryVolume")
            delete_list = self.recovery.sanity_check(recovery_volume, all_snapshots)
            self.logger.write_demon_info("Starting sanity check")
            n_del = 0
            if len(delete_list)!=0:
                for snap in delete_list:
                    env_id = self.db.get_env_from_snap_id(snap_id=snap["fs_id"])
                    if env_id != "NoValue":
                        self.db.delete_env_snapshot(env_id=env_id, snap_id=snap["fs_id"])
                    self.db.delete_snapshot(snap["fs_id"])
                    self.logger.write_demon_info("Deleted" + json.dumps(snap))
                    n_del+=1
            self.logger.write_demon_info("Sanity check ended. Deleted " + str(n_del) + " snapshots")
            self.require_sanity_check = False
            self.lock.release()

        while not self.closeEvent.isSet():
            self.lock.acquire()
            print("Daemon - lock acquired")
            environments = self.db.get_env_ids()
            if environments != "NoValue":
                print(environments)
                for env in environments:
                    details = self.db.environment_detail(env)
                    if details["status"] == "new":
                        self.control_point[details["env_id"]] = dict()
                        self.last_changes[details["env_id"]] = dict()
                        print(details["env_id"] + " ---> Init success")
                    for watcher in details["watchers"]:
                        if details["status"] == "new":
                            watcher_control_point = datetime.datetime.strptime(details["creation_date"], "%Y-%m-%d %H:%M:%S")
                            self.control_point[details["env_id"]][watcher["key"]] = watcher_control_point.strftime("%H:%M:%S")
                            audit.new_file_watcher(key=watcher["key"], file=watcher["file"],
                                                   permission_filter=watcher["permission_filter"])
                            audit_return = audit.audit_query(file=watcher["file"], key=watcher["key"], mode="date", params=self.control_point[details["env_id"]][watcher["key"]])
                            self.last_changes[details["env_id"]][watcher["key"]] = audit_return[1]
                            self.db.update_env_status(details["env_id"], "sync")
                            details = self.db.environment_detail(details["env_id"])
                            print(details["env_id"] + " ---> Env sync")
                        else:
                            audit_return = audit.audit_query(file=watcher["file"], key=watcher["key"], mode="date",
                                                             params=self.control_point[details["env_id"]][
                                                                 watcher["key"]])
                            if audit_return[0] == 0:
                                if audit_return[1] != self.last_changes[details["env_id"]][watcher["key"]]:
                                        self.last_changes[details["env_id"]][watcher["key"]]= audit_return[1]
                                        print(details["env_id"] + " ---> " + watcher["file"] +" changed. I'll make a snapshot for you")
                                        current_time = datetime.datetime.now()
                                        if self.update_environment(current_time=current_time, env_id=details["env_id"], owner=details["owner"], watcher=watcher) == 0:
                                            details = self.db.environment_detail(details["env_id"])
                                            n_snapshot = len(details["snapshots"])
                                            self.db.update_env_status(details["env_id"], "snap_" + str(n_snapshot))
                                            print(details["env_id"] + " ---> Update ok")
                                            self.closeEvent.wait(0.1)

                self.lock.release()
                print("Daemon - lock released")
                if self.closeEvent.isSet():
                    break
                else:
                    self.closeEvent.wait(self.time_sleep)
                    continue
            else:
                self.lock.release()
                self.logger.write_demon_error("Checking environments", "No environments found!")
                break

        self.lock.acquire()
        return_val = self.clean_users_environments()
        if return_val == 0:
            print("All user environment cleaned")
        elif return_val == 2:
            print("No environment to clean")
        elif return_val == 3:
            print ("No user environment to clean")
        elif return_val == 4:
            print("Impossibile ripristinare")
        else:
            print("Error in deleting user environment")
        self.lock.release()
        audit.audit_delete_watcher()
        print("Demon closed")

    def create_environment(self, current_time, owner):
        env_name =  owner + "_environment_"+ current_time.strftime("%Y-%m-%d/%H:%M:%S.%f")
        env_id = hashlib.md5(env_name.encode()).hexdigest()
        try:
            self.db.insert_environment(env_id=env_id, name=env_name, creation_date=current_time.strftime("%Y-%m-%d %H:%M:%S"), owner=owner, status="new")
            boot_snapshot_name = "environment_creating_" + current_time.strftime("%Y-%m-%d/%H:%M:%S.%f")
            snapshot_volume = self.db.get_config("SnapshotVolume")
            recovery_volume = self.db.get_config("RecoveryVolume")
            id = hashlib.sha512(boot_snapshot_name.encode()).hexdigest()
            new_snapshot = self.recovery.make_snapshot(id=id, snapshot_folder=snapshot_volume, recovery_volume=recovery_volume, user=owner)
            self.db.insert_snapshot(fs_id=new_snapshot["id"], volid=new_snapshot["volid"], name=new_snapshot["name"],
                                    type="env",
                                    creation_date=current_time.strftime("%Y-%m-%d %H:%M:%S"), owner="recovery")
            self.db.update_recovery_plan(user=owner, snap_id=new_snapshot["id"])
            self.logger.write_demon_info(boot_snapshot_name + " successfully created")
            self.db.insert_env_snapshot(env_id=env_id, snap_id=new_snapshot["id"])
            watchers = self.db.get_default_watchers(owner=owner)
            for watcher in watchers:
                self.db.insert_env_watcher(env_id=env_id, watcher_key=watcher["key"])
            if owner == "recovery":
                self.current_env_id = env_id
            return 0

        except SnapshotException as e:
            self.logger.write_demon_error("Creating recovery environment", e.get_error())
            self.closeEvent.set()
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1
        except VolidException as e:
            self.logger.write_demon_error("Creating recovery environment", e.get_error())
            self.closeEvent.set()
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1
        except MountErrorException as e:
            self.logger.write_demon_error("Creating recovery environment", e.get_error())
            self.closeEvent.set()
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1
        except UmountErrorException as e:
            self.logger.write_demon_error("Creating recovery environment", e.get_error())
            self.closeEvent.set()
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1

        except Exception as e:
            print("Errore " + str(e))
            return 1


    def clean_last_environment(self, current_time):
        last_environment = self.db.last_recovery_environment(current_time.strftime("%Y-%m-%d %H:%M:%S"))
        if last_environment != "NoValue":
            details = self.db.environment_detail(last_environment["env_id"])
            if details != "NoValue":
                for snap in  details["snapshots"]:
                    try:
                        report = self.db.get_report(snap["fs_id"])
                        snapshot_volume = self.db.get_config("SnapshotVolume")
                        recovery_volume = self.db.get_config("RecoveryVolume")
                        self.recovery.delete_snapshot(snapshot_id=snap["fs_id"], snapshot_volume=snapshot_volume, recovery_volume=recovery_volume)
                        self.logger.write_demon_info(snap["name"] + " successfully deleted")
                        if report != "NoValue":
                            self.__delete_report__(report["id"])
                            self.db.delete_report(report_id=report["id"])
                        self.db.delete_env_snapshot(last_environment["env_id"], snap_id=snap["fs_id"])
                        self.db.delete_snapshot(snap["fs_id"])

                    except DeleteException as e:
                        self.logger.write_demon_error("Cleaning last environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                        finally:
                            self.require_sanity_check = True
                            return 1

                    except MountErrorException as e:
                        self.logger.write_demon_error("Cleaning last environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.closeEvent.set()
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                        finally:
                            self.require_sanity_check = True
                            return 1
                    except UmountErrorException as e:
                        self.logger.write_demon_error("Cleaning last environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                            self.closeEvent.set()
                            self.require_sanity_check = True
                        finally:
                            return 1
                for watcher in details["watchers"]:
                    self.db.delete_env_watcher(env_id=last_environment["env_id"], watcher_key=watcher["key"])
                self.db.set_default_watchers("recovery")
                self.db.delete_environment(last_environment["env_id"])
                self.logger.write_demon_info("Recovery Environment Cleaned")
                return 0
            else:
                return 1
        else:
            return 2

    def update_environment(self, current_time, env_id, owner, watcher):
        try:
            snapshot_name = "auto_" + current_time.strftime("%Y-%m-%d/%H:%M:%S.%f")
            snapshot_volume = self.db.get_config("SnapshotVolume")
            recovery_volume = self.db.get_config("RecoveryVolume")
            id = hashlib.sha512(snapshot_name.encode()).hexdigest()
            new_snapshot = self.recovery.make_snapshot(id=id, snapshot_folder=snapshot_volume, recovery_volume=recovery_volume, user=owner)
            self.db.insert_snapshot(fs_id=new_snapshot["id"], volid=new_snapshot["volid"], name=new_snapshot["name"],
                                    type="env",
                                    creation_date=current_time.strftime("%Y-%m-%d %H:%M:%S"), owner=owner)

            if env_id in self.last_changes:
                if watcher["key"] in self.last_changes[env_id]:
                    report_id, report_name = self.__audit_reporter__(new_snapshot["name"], self.last_changes[env_id][watcher["key"]], creation_date=current_time, watcher_file=watcher["file"])
                    self.db.insert_report(report_id, report_name, new_snapshot["id"])
            self.db.insert_env_snapshot(env_id=env_id, snap_id=new_snapshot["id"])
            if owner == "recovery":
                self.db.update_recovery_plan(user=owner, snap_id=new_snapshot["id"])
            self.logger.write_demon_info(snapshot_name + " successfully created")
            return 0

        except SnapshotException as e:
            self.logger.write_demon_error("Updating Environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1
        except UmountErrorException as e:
            self.logger.write_demon_error("Updating Environment", e.get_error())
            return 1
        except VolidException as e:
            self.logger.write_demon_error("Updating Environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1
        except MountErrorException as e:
            self.logger.write_demon_error("Updating Environment", e.get_error())
            try:
                recovery_volume = self.db.get_config("RecoveryVolume")
                self.recovery.emergency_umount(recovery_volume)
            except UmountErrorException as e:
                self.logger.write_demon_error("Emergency Umount", e.get_error())
            finally:
                return 1

    def clean_users_environments(self):
        environments = self.db.get_env_ids()
        if environments != "NoValue":
            for env in environments:
                details = self.db.environment_detail(env)
                if details["owner"] != "recovery":
                    osVolume = self.db.get_config("OsVolume")
                    print(osVolume)
                    try:
                        restore_plan = self.db.get_recovery_plan(details["owner"])
                        print(restore_plan)
                        if restore_plan != "NoValue":
                            env_id = self.db.get_env_from_snap_id(restore_plan["snap_id"])
                            self.recovery.restore_snapshot(id=restore_plan["snap_id"], osVolume=osVolume, user=details["owner"])
                            self.db.delete_env_snapshot(env_id=env_id, snap_id=restore_plan["snap_id"])
                            self.db.delete_snapshot(restore_plan["snap_id"])
                            details = self.db.environment_detail(details["env_id"])
                        else:
                            return 4
                    except DeleteException as e:
                        self.logger.write_demon_error("Cleaning " + details["owner"] + " environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                        finally:
                            self.require_sanity_check = True
                            return 1

                    except MountErrorException as e:
                        self.logger.write_demon_error("Cleaning " + details["owner"] + " environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.closeEvent.set()
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                        finally:
                            self.require_sanity_check = True
                            return 1
                    except UmountErrorException as e:
                        self.logger.write_demon_error("Cleaning " + details["owner"] + " environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                            self.closeEvent.set()
                            self.require_sanity_check = True
                        finally:
                            return 1
                    except MoveException as e:
                        self.logger.write_demon_error("Restoring environment", e.get_error())
                        try:
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.emergency_umount(recovery_volume)
                        except UmountErrorException as e:
                            self.closeEvent.set()
                            self.logger.write_demon_error("Emergency Umount", e.get_error())
                        finally:
                            self.require_sanity_check = True
                            return 1

                    for snap in details["snapshots"]:
                        try:
                            report = self.db.get_report(snap["fs_id"])
                            snapshot_volume = self.db.get_config("SnapshotVolume")
                            recovery_volume = self.db.get_config("RecoveryVolume")
                            self.recovery.delete_snapshot(snapshot_id=snap["fs_id"], snapshot_volume=snapshot_volume,
                                                          recovery_volume=recovery_volume)
                            self.logger.write_demon_info(snap["name"] + " successfully deleted")
                            if report != "NoValue":
                                self.__delete_report__(report["id"])
                                self.db.delete_report(report_id=report["id"])
                            self.db.delete_env_snapshot(details["env_id"], snap_id=snap["fs_id"])
                            self.db.delete_snapshot(snap["fs_id"])

                        except DeleteException as e:
                            self.logger.write_demon_error("Cleaning " + details["owner"]+ " environment", e.get_error())
                            try:
                                recovery_volume = self.db.get_config("RecoveryVolume")
                                self.recovery.emergency_umount(recovery_volume)
                            except UmountErrorException as e:
                                self.logger.write_demon_error("Emergency Umount", e.get_error())
                            finally:
                                self.require_sanity_check = True
                                return 1

                        except MountErrorException as e:
                            self.logger.write_demon_error("Cleaning " + details["owner"]+ " environment", e.get_error())
                            try:
                                recovery_volume = self.db.get_config("RecoveryVolume")
                                self.recovery.emergency_umount(recovery_volume)
                            except UmountErrorException as e:
                                self.closeEvent.set()
                                self.logger.write_demon_error("Emergency Umount", e.get_error())
                            finally:
                                self.require_sanity_check = True
                                return 1
                        except UmountErrorException as e:
                            self.logger.write_demon_error("Cleaning " + details["owner"]+ " environment", e.get_error())
                            try:
                                recovery_volume = self.db.get_config("RecoveryVolume")
                                self.recovery.emergency_umount(recovery_volume)
                            except UmountErrorException as e:
                                self.logger.write_demon_error("Emergency Umount", e.get_error())
                                self.closeEvent.set()
                                self.require_sanity_check = True
                            finally:
                                return 1
                    for watcher in details["watchers"]:
                        self.db.delete_env_watcher(env_id=details["env_id"], watcher_key=watcher["key"])
                    self.db.set_default_watchers(details["owner"])
                    self.db.delete_environment(details["env_id"])
                    self.logger.write_demon_info(details["owner"] + "environment Cleaned")
                    return 0
            return 3
        else:
            return 2

    def __audit_reporter__(self, snapshot_name, audit_report, creation_date, watcher_file):
        report_name = "report_" + snapshot_name + watcher_file
        report_id = hashlib.md5(report_name.encode()).hexdigest()
        report_file = open(self.db_path + "/" + report_id, "w")
        report_dump = dict()
        report_dump["creation_date"] =  creation_date.strftime("%Y-%m-%d/%H:%M:%S.%f")
        report_dump["trigger"] = watcher_file
        report_dump["report"] = audit.convert_report_to_json(audit_report)
        json.dump(report_dump, report_file)
        report_file.close()
        return report_id, report_name

    def __delete_report__(self, report_id):
        os.remove(self.db_path + "/" + report_id)

    def stop(self):
        self.closeEvent.set()
