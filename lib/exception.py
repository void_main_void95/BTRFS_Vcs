class MountErrorException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["mount"] = dict()
        self.error["mount"]["code"] = error["mount"]["code"]
        self.error["mount"]["string"] = error["mount"]["string"]

    def get_error(self):
        if self.error["mount"]["code"] != 0:
            error_message = "Mount Error! Code: " + str(self.error["mount"]["code"]) + " - " + self.error["mount"]["string"]
            return error_message


class UmountErrorException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["umount"] = dict()
        self.error["umount"]["code"] = error["umount"]["code"]
        self.error["umount"]["string"] = error["umount"]["string"]

    def get_error(self):
        if self.error["umount"]["code"] != 0:
            error_message = "Mount Error! Code: " + str(self.error["umount"]["code"]) + " - " + self.error["umount"]["string"]
            return error_message


class SnapshotException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["snapshot"] = dict()
        self.error["snapshot"]["code"] = error["snapshot"]["code"]
        self.error["snapshot"]["string"] = error["snapshot"]["string"]

    def get_error(self):
        if self.error["snapshot"]["code"] != 0:
            error_message = "Snapshot Error! Code: " + str(self.error["snapshot"]["code"]) + " - " + \
                            self.error["snapshot"]["string"]
            return error_message


class VolidException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["volid"] = dict()
        self.error["volid"]["code"] = error["volid"]["code"]
        self.error["volid"]["string"] = error["volid"]["string"]

    def get_error(self):
        if self.error["volid"]["code"] != 0:
            error_message = "Volid Error! Code: " + str(self.error["volid"]["code"]) + " - " + self.error["volid"]["string"]
            return error_message


class DeleteException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["del"] = dict()
        self.error["del"]["code"] = error["del"]["code"]
        self.error["del"]["string"] = error["del"]["string"]

    def get_error(self):
        if self.error["del"]["code"] != 0:
            error_message = "Delete Error! Code: " + str(self.error["del"]["code"]) + " - " + self.error["del"]["string"]
            return error_message

class MoveException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["move"] = dict()
        self.error["move"]["code"] = error["move"]["code"]
        self.error["move"]["string"] = error["move"]["string"]

    def get_error(self):
        if self.error["del"]["code"] != 0:
            error_message = "Delete Error! Code: " + str(self.error["move"]["code"]) + " - " + self.error["move"]["string"]
            return error_message


class NotFoundException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["notFound"] = dict()
        self.error["notFound"]["code"] = error["notFound"]["code"]
        self.error["notFound"]["string"] = error["notFound"]["string"]

    def get_error(self):
        if self.error["del"]["code"] != 0:
            error_message = "Delete Error! Code: " + str(self.error["notFound"]["code"]) + " - " + self.error["notFound"]["string"]
            return error_message


class ListException(Exception):
    def __init__(self, error):
        Exception.__init__(self)
        self.error = dict()
        self.error["list"] = dict()
        self.error["list"]["code"] = error["list"]["code"]
        self.error["list"]["string"] = error["list"]["string"]

    def get_error(self):
        if self.error["list"]["code"] != 0:
            error_message = "List Error! Code: " + str(self.error["list"]["code"]) + " - " + self.error["list"]["string"]
            return error_message
