import sqlite3
import datetime

class DbInteface():

    def __init__(self, database_path):
        self.dbconn = sqlite3.connect(database_path, isolation_level=None)
        self.dbconn.execute('pragma journal_mode=wal;')
        self.dbconn.commit()
        self.dbcursor = self.dbconn.cursor()

#developmens methods
    def create_table(self):

        self.dbcursor.execute('''CREATE TABLE miscellanea(
                                 setting text,
                                 value text,
                                 PRIMARY KEY (setting, value)
                                 )
                                 ''')

        self.dbcursor.execute('''
                                 CREATE TABLE users (
                                  unix_name text PRIMARY KEY , 
                                  password text
                                  )
                                 ''')

        self.dbcursor.execute('''
                                 CREATE TABLE snapshots(
                                  fs_id text,
                                  volid text,
                                  name text,
                                  type text,
                                  creation_date date,
                                  owner text,
                                  PRIMARY KEY (fs_id, volid),
                                  FOREIGN KEY(owner) REFERENCES users(unix_name)                      
                                  )
                                  ''')

        self.dbcursor.execute('''
                                 CREATE TABLE watchers(
                                  key text,
                                  file text,
                                  permission_filter text,
                                  owner text,
                                  type text,
                                  PRIMARY KEY(key,file),
                                  FOREIGN KEY(owner) REFERENCES users(unix_name)
                                 )
                                 ''')
        self.dbcursor.execute(''' 
                                 CREATE TABLE report(
                                 id text PRIMARY KEY,
                                 name text,
                                 snapshot text,
                                 
                                 FOREIGN KEY(snapshot) REFERENCES snapshots(fs_id)
                                 )
                                  ''')
        self.dbcursor.execute('''
                                CREATE TABLE config(
                                  volume text PRIMARY KEY,
                                  device text,
                                  mount_point text,
                                  temp_folder text,
                                  volid text DEFAULT -1                              
                                )
                                ''')

        self.dbcursor.execute('''CREATE TABLE paths(
                                 label text PRIMARY KEY,
                                 absolute_path text
                                 )''')

        self.dbcursor.execute('''CREATE TABLE environments(
                                 env_id text PRIMARY KEY,
                                 name text,
                                 creation_date date,
                                 owner text,
                                 status text,
                                 FOREIGN KEY (owner) REFERENCES users (unix_name),
                                 CONSTRAINT max_one_per_user UNIQUE (env_id, owner)
                                 )''')

        self.dbcursor.execute('''CREATE TABLE environment_snapshots(
                                 env_id text,
                                 snap_id text,
                                 PRIMARY KEY (env_id, snap_id),
                                 FOREIGN KEY (snap_id) REFERENCES snapshots (fs_id),
                                 FOREIGN KEY (env_id) REFERENCES enviroments (env_id),
                                 CONSTRAINT max_one_snap UNIQUE (snap_id)
                                   )''')

        self.dbcursor.execute('''CREATE TABLE env_watchers(
                                  env_id text,
                                  watcher_key text,
                                  PRIMARY KEY (env_id, watcher_key),
                                  FOREIGN KEY (env_id) REFERENCES environments (env_id),
                                  FOREIGN KEY (watcher_key) REFERENCES watchers (key)
                                  )''')

        self.dbcursor.execute('''CREATE TABLE recovery_plan(
                                 user text PRIMARY KEY,
                                 snap_id text,
                                 FOREIGN KEY (user) REFERENCES users (unix_name),
                                 FOREIGN KEY (snap_id) REFERENCES snapshots (fs_id)
                                 )''')


        self.dbconn.commit()

    def development_create(self):
        self.insert_miscellanea("root_status", "valid")
        self.insert_miscellanea("home_status", "valid")
        self.insert_user("tesi", "12345")
        self.insert_user("recovery", "toor")
        self.insert_watcher("conf", "/etc", "wa", "recovery", "default")
        self.dbcursor.execute('''INSERT INTO recovery_plan (user) VALUES ("recovery")''')
        self.dbcursor.execute('''INSERT INTO recovery_plan (user) VALUES ("tesi")''')
        self.dbcursor.execute('''INSERT INTO config (volume, device, mount_point, temp_folder, volid )
                                    VALUES ("SnapshotVolume", "/dev/sda2", "/home/tesi/python_build", "/recovery/tmp_mnt", "265")
                                    ''')

        self.dbcursor.execute('''INSERT INTO config (volume, device, mount_point, temp_folder, volid )
                                            VALUES ("RecoveryVolume", "/dev/sda2", "/home/tesi/python_build", "/recovery/tmp_mnt", "257")
                                            ''')

        self.dbcursor.execute('''INSERT INTO config (volume, device, mount_point, temp_folder)
                                            VALUES ("OsVolume", "/dev/sda2", "/home/tesi/python_build", "/recovery/tmp_mnt")
                                            ''')

        root = "/recovery"
        self.insert_path("RecoveryRootFolder", root)
        self.insert_path("LibraryFolder", root + "/lib")
        self.insert_path("DatabaseFolder", root + "/database")
        self.insert_path("ConfigsFolder", root + "/config_files")
        self.insert_path("LogsFolder", root + "/logs")
        self.dbconn.commit()

#miscellanea methods
    def get_miscellanea(self, setting):
        if setting != "all":
            query = "SELECT * FROM miscellanea WHERE setting = ?"
            data = (setting, )
            self.dbcursor.execute(query, data)
            result = self.dbcursor.fetchone()
            if not result:
                return "NoValue"
            else:
                json_result = dict()
                json_result["setting"] = result[0]
                json_result["value"] = result[1]
                return json_result

        else:
            self.dbcursor.execute('''SELECT * FROM miscellanea''')
            result = self.dbcursor.fetchall()
            if not result:
                return "NoValue"
            else:
                json_result = list()
                for row in result:
                    miscellanea = dict()
                    miscellanea["setting"] = row[0]
                    miscellanea["value"] = row[1]
                    json_result.append(miscellanea)
                return json_result


    def insert_miscellanea(self, setting, value):
        query = "INSERT INTO miscellanea (setting, value) VALUES (?,?)"
        data = (setting, value)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True


#recovery_plan methods
    def update_recovery_plan(self, user, snap_id):
        query = "UPDATE recovery_plan " \
                "SET snap_id = ? " \
                "WHERE user = ?"
        data = (snap_id, user)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def insert_recovery_plan(self, user, snap_id):
        query = "INSERT INTO recovery_plan (user, snap_id) VALUES (?,?)"
        data = (user, snap_id)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_recovery_plan(self, user):
        query = "SELECT * FROM recovery_plan WHERE user = ?"
        data = (user, )
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        if not result:
            return "NoValue"
        else:
            json_result = dict()
            json_result["user"] = result[0]
            json_result["snap_id"] = result[1]
            return json_result


#environments methods
    def insert_environment(self, env_id, name, creation_date, owner, status):
        query = "INSERT INTO environments (env_id, name, creation_date, owner, status) VALUES (?,?,?,?,?)"
        data = (env_id, name, creation_date, owner, status)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def delete_environment(self, env_id):
        query = "DELETE FROM environments WHERE env_id = ?"
        data = (env_id, )
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def last_recovery_environment(self, current_time):
        query = "SELECT * FROM environments WHERE owner= ? and creation_date < ?"
        data = ("recovery", current_time)
        self.dbcursor.execute(query, data)
        row = self.dbcursor.fetchone()
        if not row:
            return "NoValue"
        else:
            json_result = dict()
            json_result["env_id"] = row[0]
            json_result["name"] = row[1]
            json_result["creation_date"] = row[2]
            json_result["owner"] = row[3]
            json_result["status"] = row[4]
            return json_result


    def environment_detail(self, env_id):
        query = "SELECT * FROM environments WHERE env_id = ?"
        data = (env_id, )
        self.dbcursor.execute(query,data)
        environment_description = self.dbcursor.fetchone()
        if not environment_description:
            return "NoValue"
        else:
            query = "SELECT environment_snapshots.snap_id FROM environment_snapshots JOIN snapshots " \
                    "ON environment_snapshots.snap_id = snapshots.fs_id WHERE environment_snapshots.env_id = ? ORDER BY snapshots.creation_date ASC"
            data = (environment_description[0],)
            self.dbcursor.execute(query, data)
            environment_snapshots = self.dbcursor.fetchall()
            json_result = dict()
            json_result["env_id"] = environment_description[0]
            json_result["name"] = environment_description[1]
            json_result["creation_date"] = environment_description[2]
            json_result["owner"] = environment_description[3]
            json_result["status"] = environment_description[4]
            json_result["snapshots"] = list()
            if not environment_snapshots:
                pass
            else:
                for snap in environment_snapshots:
                    details = self.get_snapshots_detail(snap[0])
                    if details != "NoValue":
                        json_result["snapshots"].append(details)
            json_result["watchers"] = list()
            query = "SELECT watchers.key, watchers.file, watchers.permission_filter, watchers.owner, watchers.type " \
                    "FROM env_watchers JOIN watchers ON env_watchers.watcher_key = watchers.key WHERE env_id = ?"
            data = (environment_description[0],)
            self.dbcursor.execute(query, data)
            watchers = self.dbcursor.fetchall()
            if not watchers:
                pass
            else:
                for row in watchers:
                    watcher = dict()
                    watcher["key"] = row[0]
                    watcher["file"] = row[1]
                    watcher["permission_filter"] = row[2]
                    watcher["owner"] = row[3]
                    watcher["type"] = row[4]
                    json_result["watchers"].append(watcher)

            return json_result

    def get_env_ids(self):
        query = "SELECT env_id FROM environments"
        self.dbcursor.execute(query)
        result = self.dbcursor.fetchall()
        if not result:
            return "NoValue"
        else:
            json_result = list()
            for row in result:
                json_result.append(row[0])
            return json_result

    def get_environment_watchers(self, env_id):
        query = "SELECT * FROM env_watchers WHERE env_id = ?"
        data = (env_id, )
        self.dbcursor.execute(query, data)
        watchers = self.dbcursor.fetchall()
        if not watchers:
            return "NoValue"
        else:
            json_result = list()
            for watcher in watchers:
                query = "SELECT * FROM watchers WHERE key = ?"
                data = (watcher[1],)
                self.dbcursor.execute(query, data)
                details = self.dbcursor.fetchone()
                if not details:
                    pass
                else:
                    watcher = dict()
                    watcher["key"] = details[0]
                    watcher["file"] = details[1]
                    watcher["permission_filter"] = details[2]
                    watcher["owner"] = details[3]
                    watcher["type"] = details[4]
                    json_result.append(watcher)
            return json_result


    def get_environments(self):
        self.dbcursor.execute('''SELECT * FROM environments''')
        result = self.dbcursor.fetchall()
        if not result:
            return "NoValue"
        else:
            json_result = list()
            for row in result:
                env = dict()
                env["env_id"] = row[0]
                env["name"] = row[1]
                env["creation_date"] = row[2]
                env["owner"] = row[3]
                env["status"] = row[4]
                json_result.append(env)

            return json_result

    def get_env_from_snap_id(self, snap_id):
        query = "SELECT env_id from environment_snapshots WHERE snap_id = ?"
        data = (snap_id, )
        self.dbcursor.execute(query, data)
        env_id = self.dbcursor.fetchone()
        if not env_id:
            return "NoValue"
        else:
            return env_id[0]


    def update_env_status(self, env_id, status):
        query = "UPDATE environments " \
                "SET status = ? " \
                "WHERE env_id = ?"
        data = (status, env_id)
        self.dbcursor.execute(query,data)
        result = self.dbcursor.fetchone()
        if not result:
            return False
        else:
            return True

#env_watchers methods
    def insert_env_watcher(self, env_id, watcher_key):
        query = "INSERT INTO env_watchers (env_id, watcher_key) VALUES (?,?)"
        data = (env_id, watcher_key)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def delete_env_watcher(self, env_id, watcher_key):
        query = "DELETE FROM env_watchers WHERE env_id = ? AND watcher_key = ?"
        data = (env_id, watcher_key)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True
    #env_snapshot methods
    def delete_env_snapshot(self, env_id, snap_id):
        query = "DELETE FROM environment_snapshots WHERE env_id = ? AND snap_id = ?"
        data = (env_id, snap_id)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def insert_env_snapshot(self, env_id, snap_id):
        query = "INSERT INTO environment_snapshots (env_id, snap_id) VALUES (?,?)"
        data = (env_id, snap_id)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

#snapshots methods
    def insert_snapshot(self, fs_id, volid, name, type, creation_date, owner):
        query = "INSERT INTO snapshots(fs_id, volid, name, type, creation_date,owner) VALUES (?,?,?,?,?,?)"
        data = (fs_id, volid, name, type, creation_date, owner)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_snapshot_list(self, owner="all"):
        if owner == "all":
            query = "SELECT * FROM snapshots"
            self.dbcursor.execute(query)

        else:
            query = "SELECT * FROM snapshots WHERE owner == ?"
            data = (owner,)
            self.dbcursor.execute(query, data)

        result = self.dbcursor.fetchall()
        if not result:
            return "NoValue"
        else:
            json_result = list()
            for row in result:
                snapshot = dict()
                snapshot["fs_id"] = row[0]
                snapshot["volid"] = row[1]
                snapshot["name"] = row[2]
                snapshot["type"] = row[3]
                snapshot["creation_date"] = row[4]
                snapshot["owner"] = row[5]
                json_result.append(snapshot)
            return json_result

    def delete_snapshot(self, snapshot_id):
        query = "DELETE FROM snapshots WHERE fs_id = ?"
        data = (snapshot_id, )
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_snapshots_detail(self, fs_id="all"):
        if fs_id == "all":
            self.dbcursor.execute("SELECT * FROM snapshots")
            result = self.dbcursor.fetchall()
            if not result:
                return "NoValue"
            else:
                json_result = list()
                for row in result:
                    details = dict()
                    details["fs_id"] = row[0]
                    details["volid"] = row[1]
                    details["name"] = row[2]
                    details["type"] = row[3]
                    details["creation_date"] = row[4]
                    details["owner"] = row[5]
                    json_result.append(details)
                return json_result
        else:
            query = "SELECT * FROM snapshots where fs_id = ?"
            data = (fs_id, )
            self.dbcursor.execute(query, data)
            row = self.dbcursor.fetchone()
            if not row:
                return "NoValue"
            else:
                details = dict()
                details["fs_id"] = row[0]
                details["volid"] = row[1]
                details["name"] = row[2]
                details["type"] = row[3]
                details["creation_date"] = row[4]
                details["owner"] = row[5]
                return details

#users methods
    def insert_user(self, unix_name, password):
        query = "INSERT INTO users(unix_name, password) VALUES(?,?)"
        data = (unix_name, password)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_users(self):
        query = "SELECT * FROM users"
        result = self.dbcursor.execute(query)
        if not result:
            return "NoValue"
        json_result = list()
        for row in result:
            user = dict()
            user["unix_name"] = row[0]
            json_result.append(user)
        return json_result

#reports methods
    def insert_report(self, id, name, snapshot):
        query = "INSERT INTO report (id, name, snapshot) VALUES (?, ?, ?)"
        data = (id, name, snapshot)
        self.dbcursor.execute(query, data)
        self.dbconn.commit()

    def get_report(self, snapshot="all"):
        if snapshot == "all":
            query = "SELECT * FROM report"
            self.dbcursor.execute(query)
            result = self.dbcursor.fetchall()
            if not result:
                return "NoValue"
            else:
                json_result = list()
                for row in result:
                    report = dict()
                    report["id"] = row[0]
                    report["name"] = row[1]
                    report["snapshot"] = row[2]
                    json_result.append(report)
                return json_result
        else:
            query = "SELECT * FROM report WHERE snapshot = ?"
            data = (snapshot,)
            self.dbcursor.execute(query, data)
            result = self.dbcursor.fetchone()
            if not result:
                return "NoValue"
            else:
                report = dict()
                report["id"] = result[0]
                report["name"] = result[1]
                report["snapshot"] = result[2]
                return report


    def delete_report(self, report_id):
        query = "DELETE FROM report WHERE id = ?"
        data = (report_id, )
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

#watchers methods
    def insert_watcher(self, key, file, permission_filter, owner, type):
        query = "INSERT INTO watchers (key, file, permission_filter, owner, type) VALUES (?,?,?,?,?)"
        data = (key, file, permission_filter, owner, type)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_watchers(self, owner="all"):
        if owner == "all":
            query = "SELECT * FROM watchers"
            self.dbcursor.execute(query)
        else:
            query = "SELECT * FROM watchers WHERE owner = ?"
            data = (owner,)
            self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchall()
        if not result:
            return "NoValue"
        else:
            json_result = list()
            for row in result:
                watcher = dict()
                watcher["key"] = row[0]
                watcher["file"] = row[1]
                watcher["permission_filter"] = row[2]
                watcher["owner"] = row[3]
                watcher["type"] = row[4]
                json_result.append(watcher)
            return json_result

    def set_default_watchers(self, owner):
        query = "DELETE FROM watchers WHERE type != ? AND owner = ?"
        data = ("default", owner)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_default_watchers(self, owner):
        query = "SELECT * FROM watchers WHERE type = ? AND owner = ?"
        data = ("default", owner)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchall()
        if not result:
            return "NoValue"
        else:
            json_result = list()
            for row in result:
                watcher = dict()
                watcher["key"] = row[0]
                watcher["file"] = row[1]
                watcher["permission_filter"] = row[2]
                watcher["owner"] = row[3]
                watcher["type"] = row[4]
                json_result.append(watcher)
            return json_result

    def delete_watcher(self, key):
        query = "DELETE FROM watchers WHERE key = ?"
        data = (key, )
        self.dbcursor.execute(query, data)
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True



#path methods
    def insert_path(self, label, absolute_path):
        query = "INSERT into paths (label, absolute_path) VALUES (?, ?)"
        data = (label, absolute_path)
        self.dbcursor.execute(query, data)
        result = self.dbcursor.fetchone()
        self.dbconn.commit()
        if not result:
            return False
        else:
            return True

    def get_path(self, label):
        if label == "all":
            query = "SELECT * FROM paths"
            self.dbcursor.execute(query)
            result = self.dbcursor.fetchall()
            if not result:
                return "NoValue"
            else:
                json_result = list()
                for row in result:
                    path = dict()
                    path["volume"] = row[0]
                    path["absolute_path"] = row[1]
                    json_result.append(path)
                return json_result

        else:
            query = "SELECT * FROM paths WHERE label = ?"
            data = (label, )
            self.dbcursor.execute(query, data)
            row = self.dbcursor.fetchone()
            if not row:
                return "NoValue"
            else:
                return row[1]

#config methods
    def get_config(self, volume="all"):
        if volume == "all":
            self.dbcursor.execute('''SELECT * FROM config''')
            result = self.dbcursor.fetchall()
            if not result:
                return "NoValue"
            else:
                json_result = list()
                for row in result:
                    config = dict()
                    config["volume"] = row[0]
                    config["device"] = row[1]
                    config["mount_point"] = row[2]
                    config["temp_folder"] = row[3]
                    config["volid"] = row[4]
                    json_result.append(config)
                return json_result
        else:
            query = "SELECT * FROM config WHERE volume = ?"
            data = (volume,)
            self.dbcursor.execute(query, data)
            row = self.dbcursor.fetchone()
            if not row:
                return  "NoValue"
            else:
                json_result = dict()
                json_result["device"] = row[1]
                json_result["mount_point"] = row[2]
                json_result["temp_folder"] = row[3]
                json_result["volid"] = row[4]
                return json_result

#security methods
    def authenticate(self, unix_name, password):
        query = "SELECT * FROM users WHERE unix_name = ?"
        data = (unix_name, )
        self.dbcursor.execute(query, data)
        row = self.dbcursor.fetchone()
        if not row:
            return False
        else:
            if password == row[1]:
                return True
            else:
                return False

    def close(self):
        self.dbconn.close()

