import subprocess


def new_file_watcher(file, key, permission_filter):
    audit_command = "auditctl -w " + file + " -p " + permission_filter + " -k " + key
    child = subprocess.Popen(audit_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child.communicate()
    child_error = child.returncode
    return child_error


def audit_query(file, key,  mode, params):
    audit_command = "ausearch -f " + file + " -k " + key
    if mode == "date":
        audit_command += " -ts " + params
    elif mode == "user":
        audit_command += " -ui " + params
    child = subprocess.Popen(audit_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child.communicate()
    child_error = child.returncode
    if child_error == 1:
        return child_error, child_stderr.decode()
    else:
        return child_error, child_stdout.decode()


def audit_delete_watcher(key="all"):
    if key =="all":
        audit_command = "auditctl -D "
    else:
        audit_command = "auditctl -D -k " + key
    child = subprocess.Popen(audit_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child.communicate()
    child_error = child.returncode
    return child_error


def convert_report_to_json(report):
    report = report.split("----\n")
    report.pop(0)
    json_report = list()
    for msg in report:
        init = True
        message = dict()
        temp = msg.split("\n")
        message["time"] = temp.pop(0)
        if temp.count("") == 1:
            temp.pop(len(temp)-1)
        message["records"] = list()
        message["last_changes"] = list()
        last_changes = dict()
        last_changes["name"] = list()
        last_changes["process"] = dict()
        for row in temp:
            records = row.split("\n")
            for record in records:
                json_record = dict()
                json_record["payload"] = dict()

                record = record.split(": ")
                preamble = record[0]
                payload = record[1]
                preamble = preamble.split(" ")
                type = preamble[0]
                type = type.split("=")
                json_record[type[0]] = type[1]
                id = preamble[1]
                id = id.split("=")
                if init:
                    message["msg_id"] = id[1]
                    init = False
                payload = payload.split(" ")
                for row in payload:
                    row = row.split("=")
                    if '"' in row[1]:
                        row[1] = row[1].replace('"', '', 2)
                    if type[1] == "PATH":
                        if row[0] == "name":
                            last_changes["name"].append(row[1])
                    if type[1] == "SYSCALL":
                        if row[0] == "exe":
                            last_changes["process"]["name"] = row[1]
                        elif row[0] == "pid":
                            last_changes["process"]["pid"] = row[1]

                    json_record["payload"][row[0]] = row[1]

                message["records"].append(json_record)

        message["last_changes"].append(last_changes)
        json_report.append(message)

    return json_report