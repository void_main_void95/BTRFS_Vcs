import hashlib
import json
import lib.btrfsWrapper as btrfs
from lib.exception import *


class BtrfsRecovery():

    def __init__(self, thread_owner):
        self.thread_owner = thread_owner

    def make_snapshot(self, id, snapshot_folder, recovery_volume, user):
        error = dict()
        error["mount"] = dict()
        error["snapshot"] = dict()
        error["volid"] = dict()
        error["umount"] = dict()
        return_value = btrfs.mount_subvolume(snapshot_folder['device'],
                                             snapshot_folder["volid"],
                                             recovery_volume['temp_folder'])
        error["mount"]["code"] = return_value[0]
        error["mount"]["string"] = return_value[1]
        if error["mount"]["code"] == 0:
            if user == "recovery":
                return_value = btrfs.new_snapshot(id, "/", recovery_volume['temp_folder'])
            else:
                return_value = btrfs.new_snapshot(id, "/home/" + user, recovery_volume['temp_folder'])
            error["snapshot"]["code"] = return_value[0]
            error["snapshot"]["string"] = return_value[1]
            if error["snapshot"]["code"] == 0:
                new_volid = btrfs.get_volid(id, "/")
                if new_volid != -1:
                    error["volid"]["code"] = 0
                    error["volid"]["string"] = "Success"
                    new_snapshot = dict()
                    new_snapshot["id"] = id
                    new_snapshot["name"] = name
                    new_snapshot["volid"] = new_volid
                else:
                    error["volid"]["code"] = -1
                    error["volid"]["string"] = "Volid not found"
                    raise VolidException(error)
            else:
                raise SnapshotException(error)
            return_value = btrfs.umount(recovery_volume['temp_folder'])
            error["umount"]["code"] = return_value[0]
            error["umount"]["string"] = return_value[1]
            if error["umount"]["code"] == -1:
                raise UmountErrorException(error)
        else:
            raise MountErrorException(error)
        return new_snapshot

    def delete_snapshot(self, snapshot_id, snapshot_volume, recovery_volume):
        error = dict()
        error["mount"] = dict()
        error["list"] = dict()
        error["del"] = dict()
        error["umount"] = dict()
        return_value = btrfs.mount_subvolume(snapshot_volume["device"],
                                             snapshot_volume["volid"],
                                             snapshot_volume["temp_folder"])
        error["mount"]["code"] = return_value[0]
        error["mount"]["string"] = return_value[1]
        if error["mount"]["code"] == 0:
            return_value = btrfs.del_subvolume(snapshot_id, "/", recovery_volume["temp_folder"])
            error["del"]["code"] = return_value[0]
            error["del"]["string"] = return_value[1]
            if error["del"]["code"] != 0:
                    raise DeleteException(error)
            return_value = btrfs.umount(recovery_volume['temp_folder'])
            error['umount']['code'] = return_value[0]
            error['umount']['string'] = return_value[1]
            if error['umount']['code'] == -1:
                raise UmountErrorException(error)
            return 0
        else:
            raise MountErrorException(error)

    def emergency_umount(self, recovery_volume):
        return_value = btrfs.umount(recovery_volume['temp_folder'])
        error = dict()
        error["umount"] = dict()
        error['umount']['code'] = return_value[0]
        error['umount']['string'] = return_value[1]
        if error['umount']['code'] == -1:
            raise UmountErrorException(error)

    def sanity_check(self, recovery_volume, all_snapshot):
        delete_list = list()
        return_value, snapshot_list = btrfs.list_snapshot(recovery_volume['temp_folder'])
        if return_value == 0:
            for snap in all_snapshot:
                if snapshot_list.count(snap["fs_id"]) == 0:
                    delete_list.append(snap)
            return delete_list
        else:
            error = dict()
            error["sanity_check"] = return_value
            return  error

    def restore_snapshot(self, id, osVolume, user):
        error = dict()
        error["mount"] = dict()
        error["move"] = dict()
        error["del"] = dict()
        error["umount"] = dict()
        snapshot_folder = osVolume["temp_folder"] + "/snapshots"
        if user == "recovery":
            current_id = "@"
            temp_id = "@.bak"
            main_folder = osVolume["temp_folder"]
        else:
            current_id = user
            temp_id = user + ".bak"
            main_folder = osVolume["temp_folder"] + "/@home"
        return_value = btrfs.mount_disk(osVolume["device"], osVolume["temp_folder"])
        error["mount"]["code"] = return_value[0]
        error["mount"]["string"] = return_value[1]
        if error["mount"]["code"] == 0:
            return_value = btrfs.move_snapshot(current_id, main_folder, main_folder, temp_id)
            error["move"]["code"] = return_value[0]
            error["move"]["string"] = return_value[1]
            if error["move"]["code"] == 0:
                return_value = btrfs.move_snapshot(id, snapshot_folder, main_folder, current_id)
                error["move"]["code"] = return_value[0]
                error["move"]["string"] = return_value[1]
                if error["move"]["code"] == 0:
                    return_value = btrfs.del_subvolume(id=temp_id, dst=main_folder, src="null")
                    error["del"]["code"] = return_value[0]
                    error["del"]["string"] = return_value[1]
                    if error["del"]["code"] == 0:
                        return_value = btrfs.umount(osVolume["temp_folder"])
                        error["umount"]["code"] = return_value[0]
                        error["umount"]["string"] = return_value[1]
                        if error["umount"]["code"] == 0:
                            return "restoreSuccess"
                        else:
                            raise UmountErrorException(error)
                else:
                    raise DeleteException(error)
            else:
                raise MountErrorException(error)
        else:
            raise MountErrorException(error)