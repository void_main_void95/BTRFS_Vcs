import datetime


class DaemonLogger():
    def __init__(self, path):
        try:
            self.log_path = path
            self.demon_log = open(self.log_path + "/daemon.log", "a")
        except IOError:
            raise IOError("daemon_log not found!")

    def write_demon_error(self, action, message):
        error_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        error_message = error_time + " >>>>>> Error in " + action + "! Details: " + message + "\n"
        self.demon_log.write(error_message)
        self.demon_log.flush()

    def write_demon_info(self, message):
        info_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        info_message = info_time + " >>>>>> " + message + "\n"
        self.demon_log.write(info_message)
        self.demon_log.flush()

    def close(self):
        self.demon_log.flush()
        self.demon_log.close()

class UserClientLogger():
    def __init__(self):
        try:
            self.user_client_log = open("./logs/userInterface.log", "a")
        except IOError:
            raise IOError("userClient.log not found!")

    def write_request_resource_info(self, user, resource):
        info_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        info_message = info_time + " >>>>>>> User: "+ user + " required: " + resource + "\n"
        self.user_client_log.write(info_message)
        self.user_client_log.flush()

    def write_write_info(self, message):
        info_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        info_message = info_time + " >>>>>>> " + message + "\n"
        self.user_client_log.write(info_message)
        self.user_client_log.flush()

    def write_user_client_error(self, action, message):
        info_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        info_message = info_time + " >>>>>>> Error in: "+ action + "details: " + message +  "\n"
        self.user_client_log.write(info_message)
        self.user_client_log.flush()

    def close(self):
        self.api_log.flush()
        self.api_log.close()


class MonitorLogger():
    def __init__(self, path):
        try:
            self.log_path = path
            self.monitor_log = open(self.log_path + "/monitor.log", "a")
        except IOError:
            raise IOError("monitor.log not found!")

    def write_monitor_info(self, message):
        info_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        info_message = info_time + " >>>>>> " + message + "\n"
        self.monitor_log.write(info_message)
        self.monitor_log.flush()

    def write_monitor_error(self, action, message):
        error_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        error_message = error_time + " >>>>>> Error in " + action + "! Details: " + message + "\n"
        self.monitor_log.write(error_message)
        self.monitor_log.flush()

    def close(self):
        self.monitor_log.flush()
        self.monitor_log.close()

class RecoveryMenuLogger():
    def __init__(self, path):
        try:
            self.log_path = path
            self.monitor_log = open(self.log_path + "/recovery_menu.log", "a")
        except IOError:
            raise IOError("monitor.log not found!")

    def write_recovery_menu_info(self, message):
        info_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        info_message = info_time + " >>>>>> " + message + "\n"
        self.monitor_log.write(info_message)
        self.monitor_log.flush()

    def write_recovery_menu_error(self, action, message):
        error_time = datetime.datetime.now().strftime("%Y-%m-%d/%H:%M:%S")
        error_message = error_time + " >>>>>> Error in " + action + "! Details: " + message + "\n"
        self.monitor_log.write(error_message)
        self.monitor_log.flush()

    def close(self):
        self.monitor_log.flush()
        self.monitor_log.close()
