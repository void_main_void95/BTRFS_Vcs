import subprocess


def new_snapshot(id, src, dst):
        btrfs_command = "btrfs subvolume snapshot " + src + " " + dst + "/" + id
        child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        if child_return == 0:
            return child_return, child_stdout.decode()
        else:
            return child_return, child_stderr.decode()

def file_snapshot(id, src, dst):
    child_return, child_out = new_snapshot(id, src, dst)
    if child_return == 0:
        child_return, child_out = subvolume_to_file(id, src, dst)
        if child_return == 0:
            child_return, child_out = del_subvolume(id, src, dst)
            if child_return == 0:
                return child_return, child_out
            else:
                return child_return, child_out
        else:
            return child_return, child_out
    else:
        return child_return, child_out


def subvolume_to_file(id, src, dst):
    btrfs_command = "btrfs send " +  src + "/" + id + " -f " + dst+ "/" + id + "_file"
    child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child_process.communicate()
    child_return = child_process.returncode
    if child_return == 0:
        return child_return, child_stdout.decode()
    else:
        return child_return, child_stderr.decode()


def new_subvolume(id , src, dst):
        btrfs_command = "btrfs subvolume create " + dst + "/" + id
        child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        if child_return == 0:
            return child_return, child_stdout.decode()
        else:
            return child_return, child_stderr.decode()


def del_subvolume(id, src, dst):
        btrfs_command = "btrfs subvolume delete " + dst + "/" + id
        child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        if child_return == 0:
            return child_return, child_stdout.decode()
        else:
            return child_return, child_stderr.decode()

def del_current_root(dst):
        btrfs_command = "btrfs subvolume delete" + dst + "/@"
        child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        if child_return == 0:
            return child_return, child_stdout.decode()
        else:
            return  child_return, child_stderr.decode()

def list_snapshot(path):
        snapshot_list = []
        btrfs_command = "btrfs subvolume list -s " + path
        child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        if child_return == 0:
            child_stdout = child_stdout.decode()
            child_stdout = child_stdout.split("\n")
            for i in child_stdout:
                if i != "":
                    row = i.split(" ")
                    row = row[len(row) - 1]
                    if row != "@":
                        snapshot_list.append(row)
            return child_return, snapshot_list
        else:
            return child_return, child_stderr


def get_volid(id, path):
        btrfs_command = "btrfs subvolume list " + path
        child_process = subprocess.Popen(btrfs_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        btrfs_list = child_stdout.decode()
        btrfs_list = btrfs_list.split("\n")
        subvolid = -1
        if child_return == 0:
                for i in btrfs_list:
                    if i.find(id) != -1:
                        find_row = i.split(" ")
                        subvolid = find_row[1]

                return subvolid
        else:
                return child_return


def move_snapshot(id, src, dst, new_id):
    move_command = "mv " + src + "/" + id + " " + dst + "/" + new_id
    child_process = subprocess.Popen(move_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child_process.communicate()
    child_return = child_process.returncode
    if child_return == 0:
        return child_return, child_stdout.decode()
    else:
        return child_return, child_stderr.decode()


def mount_subvolume(device, subvolid, point):
    mount_command = "mount -o subvolid=" + str(subvolid) + " " + device + " " + point
    child_process = subprocess.Popen(mount_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child_process.communicate()
    child_return = child_process.returncode
    if child_return == 0:
        return child_return, child_stdout.decode()
    else:
        return child_return, child_stderr.decode()


def mount_disk(device, point):
    mount_command = "mount " + device + " " + point
    child_process = subprocess.Popen(mount_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    child_stdout, child_stderr = child_process.communicate()
    child_return = child_process.returncode
    if child_return == 0:
        return child_return, child_stdout.decode()
    else:
        return child_return, child_stderr.decode()


def umount(mount_point):
        umount_command = "umount " + mount_point
        child_process = subprocess.Popen(umount_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        child_stdout, child_stderr = child_process.communicate()
        child_return = child_process.returncode
        if child_return == 0:
            return child_return, child_stdout.decode()
        else:
            return child_return, child_stderr.decode()

